﻿using UnityEngine;
using Prototype.NetworkLobby;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class NetworkLobbyHook : LobbyHook 
{
    public static Dictionary<PlayerRuntimeData, LobbyPlayer> players = new Dictionary<PlayerRuntimeData, LobbyPlayer>();
    LobbyManager manager;
    private int initPlayers;

    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();

        players.Add(gamePlayer.GetComponent<PlayerRuntimeData>(), lobby);
        initPlayers++;

        if (initPlayers == manager.numPlayers)
        {
            this.manager = manager.GetComponent<LobbyManager>();

            SortList();

            ReasignGamemodes();
        }
    }

    private int GetFactionCount(Faction faction)
    {
        int aux = 0;

        foreach (KeyValuePair<PlayerRuntimeData, LobbyPlayer> data in players)
        {
            if (data.Value.playerFaction.Equals(faction))
                aux++;
        }

        return aux;
    }

    private List<LobbyPlayer> GetPlayersInFaction(Faction faction)
    {
        List<LobbyPlayer> aux = new List<LobbyPlayer>();

        foreach (KeyValuePair<PlayerRuntimeData, LobbyPlayer> data in players)
        {
            if (data.Value.playerFaction.Equals(faction))
                aux.Add(data.Value);
        }

        return aux;
    }

    private void SortList()
    {
        if (GetFactionCount(Faction.Anglosaxon) > 0)
        {
            SelectDefenderFromDefenders();
        }
        else
        {
            SelectDefenderFromAttackers();
        }
    }

    private void SelectDefenderFromAttackers()
    {
        GetPlayersInFaction(Faction.Viking)[Random.Range(0, GetPlayersInFaction(Faction.Viking).Count)].playerFaction = Faction.Anglosaxon;
    }

    private void SelectDefenderFromDefenders()
    {
        LobbyPlayer defender = GetPlayersInFaction(Faction.Anglosaxon)[Random.Range(0, GetPlayersInFaction(Faction.Anglosaxon).Count)];

        foreach (KeyValuePair<PlayerRuntimeData, LobbyPlayer> data in players)
        {
           if(data.Value != defender)
            {
                data.Value.playerFaction = Faction.Viking;
            }
        }
    }

    private void ReasignGamemodes()
    {
        foreach (KeyValuePair<PlayerRuntimeData, LobbyPlayer> data in players)
        {
            data.Key.name = data.Value.playerName;
            data.Key.pname = data.Value.playerName;
            data.Key.faction = data.Value.playerFaction;
        }
    }


    /*

    if (mode.Equals(GameModes.Attacker))
    {
        GameObject prefab = Resources.Load<GameObject>("GameModes/Attacker/Warrior");
        player = Instantiate(prefab);
    }
    else
    {
        GameObject prefab = Resources.Load<GameObject>("GameModes/Defender/Defender");
        player = Instantiate(prefab);
    }


    PlayerRuntimeData gm = player.GetComponent<PlayerRuntimeData>();

    gm.pname = lobby.playerName;
    gm.mode = mode;

    NetworkServer.Spawn(player);
    NetworkServer.ReplacePlayerForConnection(lobby.connectionToClient, player, 0);
}
*/

}
