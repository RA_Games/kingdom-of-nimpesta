﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// I phases, contain caracteristics of planning phase and Assault phase.
/// </summary>

public interface IPhases
{
	int currentTime { get; set; }

	void Add(int time);

	void EndPhase();
}
