﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

/// <summary>
/// Round manager.
/// </summary>
public class RoundManager : NetworkBehaviour
{
    [SyncVar]
    public int currentRound;
    [SyncVar]
    public int currentTime;

    public Text timer_Text;
    public Animator anim;

    public int maxRounds = 6;

    public int planningPhaseTime;
    public int assaultPhaseTime;

    public Phases phase;

    public GameObject finalPerdiste, finalGanaste;


    GameManager gameManager;

    public IEnumerator StartGame(GameManager manager)
    {
        manager.status = "Starting round manager...";

        InitRoundManager(manager.myPlayer.faction);

        gameManager = manager;
        manager.myPlayer.SetPlayerReady();
        manager.myPlayer.ActivePlayer();

        transform.GetComponentInParent<CameraPlayer>().StartFollow();

        yield return new WaitForSeconds(3);

        StartPlanningPhase();

        StartCoroutine(Cronometer());
    }

    [Command]
    public void CmdChangePhases()
    {
        switch (phase)
        {
            case Phases.Planning:
                RpcSendChangePase(Phases.Assault);
                break;
            case Phases.Assault:
                RpcSendChangePase(Phases.Planning);
                break;
        }
    }

    [ClientRpc]
    private void RpcSendChangePase(Phases p)
    {
        switch (p)
        {
            case Phases.Planning:
                StartPlanningPhase();
                break;
            case Phases.Assault:
                StartAssaultPhase();
                break;
        }
    }

    public void InitRoundManager(Faction faction)
    {
        if (faction.Equals(Faction.Anglosaxon))
        {
            anim = transform.Find("Deffensive").GetComponent<Animator>();
        }
        else
        {
            anim = transform.Find("Attacker").GetComponent<Animator>();
        }
    }

    /// <summary>
    /// Starts the assault phase.
    /// </summary>
    private void StartAssaultPhase()
    {
        phase = Phases.Assault;
        anim.SetTrigger("Assault");

        gameManager.PlayerManager.AssaultMode();

        currentTime = assaultPhaseTime;
    }

    /// <summary>
    /// Starts the planning phase.
    /// </summary>
    private void StartPlanningPhase()
    {
        gameManager.status = "Starting planning phase";
        phase = Phases.Planning;
        anim.SetTrigger("Planning");

        gameManager.PlayerManager.PlanningMode();

        currentRound++;
        currentTime = planningPhaseTime;
    }

    public string GetCurrentRound()
    {
        return currentRound.ToString();
    }


    IEnumerator Cronometer()
    {
        yield return new WaitForSeconds(2.5f);

        do
        {
            yield return new WaitForSeconds(1);
            ChangeClock(currentTime);

            if (gameManager.myPlayer.isServer)
            {
                currentTime--;

                if (currentTime <= 0)
                {
                    CmdChangePhases();

                    yield return new WaitForSeconds(5);
                }
            }
        } while (currentRound < maxRounds);

        EndGame();
    }

    public void ChangeClock(int currentTime)
    {
        timer_Text.text = GetFormatter(currentTime);
    }

    private string GetFormatter(int seconds)
    {
        int sec;
        int min;
        min = seconds / 60;
        sec = seconds % 60;

        string result = (min + " : " + sec);

        return result;

    }

    public void EndGame()
    {
        if (gameManager.myPlayer.faction.Equals(Faction.Viking) )
        {
            finalPerdiste.SetActive(true);
        }
        else if (gameManager.myPlayer.faction.Equals(Faction.Anglosaxon))
        {
            finalGanaste.SetActive(false);
        }

    }

    public static void ElGaboSeLaComeDobleda()
    {
        print("el gabo se la come doblada :P");
    }

    public enum Phases
    {
        Assault,
        Planning
    }

}
