﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public PlayerRuntimeData myPlayer;
    public Animator myHud;

    private PlayerRuntimeData[] allPlayers;

    private PlayerManager pm;
    public RoundManager myRoundManager;

    private Animator myCinematic;
    private static GameManager _singleton;

    public string status;
    public string hostStatus = "No host found";

    public static GameManager Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<GameManager>();

            return _singleton;
        }
    }

    public PlayerManager PlayerManager
    {
        get
        {
            if (pm == null)
            {
                pm = myPlayer.GetComponent<PlayerManager>();
                status = "Setting playermanager: " + pm;
            }

            return pm;
        }
    }

    public void InitGameManager() {

        allPlayers = FindObjectsOfType<PlayerRuntimeData>();

        status = "Initializing game manager";

        foreach (PlayerRuntimeData pr in allPlayers)
        {
            if (pr.hasAuthority)
            {
                myPlayer = pr;
                break;
            }
        }

        myCinematic = GetComponent<Animator>();

        if (myPlayer.faction.Equals(Faction.Anglosaxon))
        {
            myCinematic.SetTrigger("Anglosaxon");
        }

        else
        {
            myCinematic.SetTrigger("Viking");
        }
    }

    public void SetAttackerView()
    {
        Camera.main.cullingMask = ~(1 << LayerMask.NameToLayer("Defender"));
    }

    public void SetDeffenderView()
    {
        Camera.main.cullingMask = ~(1 << LayerMask.NameToLayer("Attacker"));
    }

    public void SetAttackerHUD()
    {
        status = "Setting HUD";

        myHud = transform.Find("Attacker_HUD").GetComponent<Animator>();
        myHud.SetTrigger("FirstShow");

        myCinematic.enabled = false;
        InitRoundManager();
    }

    public void SetDefenderHUD()
    {
        status = "Setting HUD";

        myHud = transform.Find("Defender_HUD").GetComponent<Animator>();
        myHud.SetTrigger("FirstShow");
        myCinematic.enabled = false;
        InitRoundManager();
    }

    public void InitRoundManager()
    {
        status = "Initializing Round Manager";

        myRoundManager = GetComponentInChildren<RoundManager>();

        StartCoroutine(myRoundManager.StartGame(this));
    }

    public bool IsAllPlayerReady()
    {
        foreach (PlayerRuntimeData pd in allPlayers)
        {
            if (!pd.isReady)
            {
                return false;
            }
        }

        return true;
    }

    private void OnGUI()
    {
        return;

        Rect rect = new Rect(0, 0, 225, 160);

        try
        {
            GUI.Box(rect, "Player: " + myPlayer + "\nManager: " + pm);
            rect.y += 120;
        }
        catch { }

        try
        {
            GUI.Box(rect, "Round Manager: " + myRoundManager);
            rect.y += 120;
        }
        catch { }

        try
        {
            GUI.Box(rect, "Cinematic: " + myCinematic);
            rect.y += 120;
        } catch { }

        try
        {
            GUI.Box(rect, "Players conected: " + allPlayers.Length);
            rect.y += 120;
        }
        catch { }

        try
        {
            GUI.Box(rect, "HUD: " + myHud);
            rect.y += 120;
        } catch { }

        try
        {
            GUI.Box(rect, "Status: " + status);
            rect.y += 120;
        } catch { }

        try
        {
            GUI.Box(rect, "Host: " + hostStatus);
        }catch { }
    }
}

public enum Faction
{
    Viking,
    Anglosaxon
}
