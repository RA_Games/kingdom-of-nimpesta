﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MaterialController : MonoBehaviour {

    [Range(0,1)]
    public float value = 0.5f;

    public Material[] materials;

    private void Start()
    {
        foreach (Material m in materials)
        {
            if (m != null)
            {
                Color color = m.color;

                color.a = value;

                m.color = color;
            }
        }
    }

    void Update () {

        foreach (Material m in materials)
        {
            if (m != null)
            {
                Color color = m.color;

                color.a = value;

                m.color = color;
            }
        }
	}
}
