﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displaies Attacker HUT.
/// </summary>
public class AttackerHUD : MonoBehaviour {


    /*
	/// <summary>
	/// Indicates the Mode of attack setted to the Troop
	/// </summary>
	private Image troopState_Image;

	/// <summary>
	/// Image of the Skills of the Troop
	/// </summary>
	private Image skill1_Image;
	private Image skill2_Image;
	private Image skill3_Image;

	/// <summary>
	/// Image that indicate the CoolDown of the Skills of the Troop
	/// </summary>
	private Image skillCDMask1_Image;
	private Image skillCDMask2_Image;
	private Image skillCDMask3_Image;

	/// <summary>
	/// Image of the pointer in the middle of the Screen.
	/// </summary>
	private Image target_Image;


	void Start () {

		// asociate Object to the image to display Info of the Mode of attack setted to the Troop
		troopState_Image = transform.Find ("TroppState_image").GetComponent<Image> ();

		// asociate Objects to the images to display Info of the Skills of the Troop
		skill1_Image = transform.Find ("Skills").Find ("Skill1_image").GetComponent<Image> ();
		skill2_Image = transform.Find ("Skills").Find ("Skill2_image").GetComponent<Image> ();
		skill3_Image = transform.Find ("Skills").Find ("Skill3_image").GetComponent<Image> ();

		// asociate Objects to the images to display info of the CoolDown of the Skills od the Troop
		skillCDMask1_Image = transform.Find ("SkillsCoolDown").Find ("SkillCDMask1_panel").GetComponent<Image> ();
		skillCDMask2_Image = transform.Find ("SkillsCoolDown").Find ("SkillCDMask2_panel").GetComponent<Image> ();
		skillCDMask3_Image = transform.Find ("SkillsCoolDown").Find ("SkillCDMask3_panel").GetComponent<Image> ();

		// asociate Object to the image to display info of the Pointer in the middle of the Screen
		target_Image = transform.Find ("Target").GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Sets the image to represent the Mode of attack setted to the Troop.
	/// </summary>
	/// <param name="sprite">Sprite.</param>
	public void setTroopStateImage (Sprite sprite)
	{
		troopState_Image.sprite = sprite;
	}
    */
}
