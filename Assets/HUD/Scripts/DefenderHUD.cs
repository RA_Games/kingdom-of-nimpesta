﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Display the Defender HUD.
/// </summary>
public class DefenderHUD : MonoBehaviour
{
    //Text of the HUD
    public Text woodAmountText;
    public Text rockAmountText;
    public Text foodAmountText;
    public Text goldAmountText;

	//Buttons of the HUD
	private Button showResources_Button;
	private Button showUnits_Button;
	private Button showSections_Button;
	private Button normalMap_Button;

	//Image of the Map
	private Image map_Image;

	//Booleans associated to the Buttons
	private bool isShowingResources = false;
	private bool isShowingSections = false;
	private bool isShowingUnits = false;

	//Colors applied to the Map associated to the Buttons
	private Color showingResources_Color = Color.red;
	private Color showingUnits_Color = Color.blue;
	private Color showingSections_Color = Color.green;

	// Use this for initialization
	void Start () 
	{
        /*
		//asocciate Objects to Buttons int he HUD to change Map
		showResources_Button = transform.Find ("MapHUD_panel").Find ("ChangeMap_panel").Find ("Resources_button").GetComponent<Button> ();
		showUnits_Button = transform.Find ("MapHUD_panel").Find ("ChangeMap_panel").Find ("Units_button").GetComponent<Button> ();
		showSections_Button = transform.Find ("MapHUD_panel").Find ("ChangeMap_panel").Find ("Sections_button").GetComponent<Button> ();
		normalMap_Button = transform.Find ("MapHUD_panel").Find ("ChangeMap_panel").Find ("Normal_button").GetComponent<Button> ();

		//asocciate Object to Image of the Map
		map_Image = transform.Find ("MapHUD_panel").Find("Map_image").GetComponent<Image> ();

		//set actions to the Buttons
		showResources_Button.onClick.AddListener (ToggleResourcesMapColor);
		showUnits_Button.onClick.AddListener (ToggleUnitsMapColor);
		showSections_Button.onClick.AddListener (ToggleSectionsMapColor);
		normalMap_Button.onClick.AddListener (ToggleMapDefault);
        */
	}

    public void Display(DefensorStats stats)
    {
        DisplayText(stats.wood.Amount, woodAmountText);
        DisplayText(stats.rock.Amount, rockAmountText);
        DisplayText(stats.food.Amount, foodAmountText);
    }

    /// <summary>
    /// Display Resource value how text
    /// </summary>
    /// <param name="value"></param>
    /// <param name="text"></param>
    private void DisplayText (int value, Text text)
    {
        text.text = value.ToString();
    }

    //Map HUD

    /// <summary>
    /// Disable All Map Options
    /// </summary>
    private void ToggleMapDefault ()
	{
		isShowingResources = true;
		isShowingUnits = true;
		isShowingSections = true;
		ToggleResourcesMapColor ();
		ToggleSectionsMapColor ();
		ToggleUnitsMapColor ();
		ChangeMap ();
	}

	/// <summary>
	/// Change show Resources info on Map Option.
	/// </summary>
	private void ToggleResourcesMapColor ()
	{
		isShowingResources = !isShowingResources;
		if(isShowingResources)
		{
			showingResources_Color = Color.green;
		}
		else
		{
			showingResources_Color = Color.white;
		}
		ChangeMap ();
	}

	/// <summary>
	/// Change show Units info on Map Option.	
	/// </summary>
	private void ToggleUnitsMapColor ()
	{
		isShowingUnits = !isShowingUnits;
		if(isShowingUnits)
		{
			showingUnits_Color = Color.blue;
		}
		else
		{
			showingUnits_Color = Color.white;
		}
		ChangeMap ();
	}

	/// <summary>
	/// Change show Sections info on Map Option.
	/// </summary>
	private void ToggleSectionsMapColor ()
	{
		isShowingSections = !isShowingSections;
		if(isShowingSections)
		{
			showingSections_Color = Color.red;
		}
		else
		{
			showingSections_Color = Color.white;
		}
		ChangeMap ();
	}

	//Apply Options to the Map in the HUD
	private void ChangeMap ()
	{
		map_Image.color = CombineColors (showingResources_Color,showingUnits_Color,showingSections_Color);
	}

	//Change Color of the Map in the HUD acoording to the Options
	private static Color CombineColors(params Color[] aColors)
	{
		Color result = new Color(0,0,0,0);
		foreach(Color c in aColors)
		{
			result += c;
		}
		result /= aColors.Length;
		return result;
	}
}