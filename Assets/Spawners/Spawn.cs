﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Region))]
public class Spawn : MonoBehaviour {

    public Texture selected;
    public Texture unSelected;
    public Vector3 flag;
    public static Spawn selectedSpawn;

    private Region myRegion;

    private bool isShowing;
    private float fadeDelta;

    private void Start()
    {
        myRegion = GetComponent<Region>();
    }

    private void OnDrawGizmos()
    {
        Color color = Color.gray;
        color.a = 0.5f;

        Gizmos.color = color;
        Gizmos.DrawSphere(flag, 20);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(flag, new Vector3(flag.x + 20,flag.y,flag.z));
        Gizmos.DrawLine(flag, new Vector3(flag.x - 20, flag.y, flag.z));

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(flag, new Vector3(flag.x, flag.y, flag.z + 20));
        Gizmos.DrawLine(flag, new Vector3(flag.x, flag.y, flag.z - 20));
    }

    public Vector3 GetSpawnPos()
    {
        Vector3 aux = flag;

        aux.x += Random.Range(-20,20);
        aux.z += Random.Range(-20, 20);

        return aux;
    }

    public static void ShowSpawns()
    {
        foreach (Spawn s in FindObjectsOfType<Spawn>())
        {
            s.ShowButton();
        }
    }

    public static void Hidepawns()
    {
        foreach (Spawn s in FindObjectsOfType<Spawn>())
        {
            s.HideButton();
        }
    }

    public void ShowButton()
    {
        isShowing = true;
    }

    public void HideButton()
    {
        isShowing = false;
    }

    private void OnGUI()
    {
        if (isShowing && myRegion.owner.Equals(Faction.Viking))
            Displaing();
        else
            Hiding();
    }

    private void Displaing()
    {
        fadeDelta += 0.03f;
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, Mathf.Clamp01(fadeDelta));

        var pos = Camera.main.WorldToScreenPoint(GetCenter());
        Vector2 size = new Vector2(20, 20);
        var pointInGUISpace = new Vector3(pos.x, Screen.height - pos.y, pos.z);

        Rect rect = new Rect(pointInGUISpace, size);

        if (GUI.Button(rect, GetState()))
        {
            Select();
            return;
        }
    }

    private Texture GetState()
    {
        if (selectedSpawn == null)
            return unSelected;

        if (selectedSpawn == this)
        {
            return selected;
        }

        return unSelected;
    }

    private void Hiding()
    {
        if (fadeDelta <= 0)
        {
            return;
        }

        fadeDelta -= 0.03f;
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, Mathf.Clamp01(fadeDelta));

        var pos = Camera.main.WorldToScreenPoint(GetCenter());
        Vector2 size = new Vector2(20, 20);
        var pointInGUISpace = new Vector3(pos.x, Screen.height - pos.y, pos.z);

        Rect rect = new Rect(pointInGUISpace, size);

        if (GUI.Button(rect, GetState()))
        {
            return;
        }
    }

    private void Select()
    {
        if (selectedSpawn == null)
        {
            selectedSpawn = this;
            FindObjectOfType<AttackerManager>().SpawnInMap();
            return;
        }

        if (selectedSpawn != this)
        {
            selectedSpawn = this;
            FindObjectOfType<AttackerManager>().SpawnInMap();
            return;
        }

        if (selectedSpawn == this)
        {
            selectedSpawn = null;
            FindObjectOfType<AttackerManager>().SpawnInMap();
            return;
        }
    }

    private Vector3 GetCenter()
    {
        return GetComponent<MeshCollider>().bounds.center;
    }
}
