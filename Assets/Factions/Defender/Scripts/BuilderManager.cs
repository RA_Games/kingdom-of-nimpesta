﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BuilderManager : NetworkBehaviour {

    private static BuilderManager _singleton;
    private bool canSpawn = false;

    private GameObject cursor;
    private Camera cam;

    private bool showAdvertencies;
    private string advertencie;

    private GameObject building_preview;
    private GameObject building_final;
    private string currentBuilding;

    private DefensorStats _defensorStats;

    public static BuilderManager Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<BuilderManager>();

            return _singleton;
        }
    }

    public DefensorStats DefenderStats
    {
        get
        {
            if (_defensorStats == null)
                _defensorStats = GetComponent<DefensorStats>();

            return _defensorStats;
        }
    }

    void Update()
    {
        if (!canSpawn)
            return;

        if (!hasAuthority)
            return;

        ValidateBuild();
    }

    public BuilderManager GetBuilderManager()
    {
        return Singleton;
    }

    public bool IsBuilding()
    {
        return canSpawn;
    }

    public void ActiveSpawn()
    {
        canSpawn = true;
    }

    public void DisableSpawn()
    {
        canSpawn = false;
        HideCursor();
    }

    private void ValidateBuild()
    {
        RaycastHit hit;

        Ray ray = GetCamera().ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            GetCursor().transform.position = hit.point;

            building_preview.transform.position = hit.point;

            if (hit.collider.tag.Equals("Buildible"))
            {
                ChangePreviewColor(Color.green, building_preview);

                if (Input.GetMouseButtonDown(0))
                {
                    Build(hit.point);
                }
            }
            else
            {
                ChangePreviewColor(Color.red, building_preview);
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            HideCursor();
            Singleton.DisableSpawn();
        }  
    }

    public void Build(Vector3 pos)
    {
        GameObject building_prefab = Resources.Load<GameObject>("Buildings/Towers/Arrow_Tower/Final");

        Constructible building = building_prefab.GetComponent<Constructible>();
        Singleton.StartCoroutine(Singleton.ShowAdvertencie("Building tower..."));

        if (DefenderStats.wood.CanUse(building.woodCost))
        {
            if (DefenderStats.rock.CanUse(building.rockCost))
            {
                if (DefenderStats.food.CanUse(building.foodCost))
                {
                    Singleton.StartCoroutine(Singleton.ShowAdvertencie("Building..."));
                    DefenderStats.wood.Use(building.woodCost);
                    DefenderStats.rock.Use(building.rockCost);
                    DefenderStats.food.Use(building.foodCost);
                    Singleton.CmdSpawnObject(building_prefab, pos);
                }
                else
                {
                    Singleton.StartCoroutine(Singleton.ShowAdvertencie("You need " + (building.foodCost - DefenderStats.food.Amount) + "of food"));
                }
            }
            else
            {
                Singleton.StartCoroutine(Singleton.ShowAdvertencie("You need " + (building.rockCost - DefenderStats.rock.Amount) + "of rock"));
            }
        }
        else
        {
            Singleton.StartCoroutine(Singleton.ShowAdvertencie("You need " + (building.woodCost - DefenderStats.wood.Amount) + "of wood"));
        }
    }

    public void SelectTowerToBuild(TowerTypes tower)
    {
        building_preview = Instantiate(Resources.Load<GameObject>("Buildings/Towers/Arrow_Tower/ArrowTower_Preview"));
        currentBuilding = tower.ToString();

        if (building_preview == null)
            DisableSpawn();
    }

    private GameObject GetCursor()
    {
        if(cursor == null)
        {
            cursor = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            cursor.GetComponent<Renderer>().material.color = Color.green;
            cursor.transform.localScale *= 3;
            Destroy(cursor.GetComponent<SphereCollider>());
            Destroy(cursor.GetComponent<MeshRenderer>());
            cursor.name = "Cursor";
        }

        cursor.SetActive(true);
        return cursor;
    }

    private void ChangePreviewColor(Color color, GameObject preview)
    {
        preview.GetComponent<Renderer>().material.color = color;
    }

    private Camera GetCamera()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }

        return cam;
    }

    private void HideCursor()
    {
        if (cursor == null)
            cursor = GetCursor();

        Destroy(building_preview);
        cursor.SetActive(false);
    }

    private IEnumerator ShowAdvertencie(string adv)
    {
        advertencie = adv;

        showAdvertencies = true;

        yield return new WaitForSeconds(3);

        showAdvertencies = false;
    }

    private void OnGUI()
    {
        if (showAdvertencies)
        {
            GUIStyle style = new GUIStyle()
            {
                fontSize = 42,
                alignment = TextAnchor.MiddleCenter,
            };

            style.normal.textColor = Color.white;
            style.hover.textColor = Color.white;

            Rect rect = new Rect(Screen.width/2, Screen.height/2, 150, 40);

            GUI.Box(rect, advertencie, style);
        }
    }

    [Command]
    public void CmdSpawnObject(GameObject prefab, Vector3 position)
    { 
        var build = Instantiate(prefab, position, Quaternion.identity);
        NetworkServer.Spawn(build);
    }
}

public class BuildingDataBase
{

}

public enum TowerTypes
{
    Arrow_Tower
}
