﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderManager : PlayerManager
{
    public float speed = 100.0f;
    public float relationDistance = 0.666f;
    public float relationHeight = 0.5f;
    public float variantSpeed = 2f;
    public float height = 100f;
    public float scrollSpeed = 1f;
    public float rotateSpeed = 40;
    public Vector3 centerMap = new Vector3(550,0,820);

    private Vector3 nextPosition;
    private bool onMap = true;

    private float initSpeed, bonusSpeed = 0f;
    private float distanceX, distanceY;
    private float heightMax, heightMin;
    private float zoomVariant;

    private void Awake()
    {
        GetComponent<DefensorStats>().Init();
    }

    private void Start()
    {
        initSpeed = speed;

        this.transform.position = new Vector3(centerMap.x,500,centerMap.z); //<<< temp

        distanceX = Screen.width / 2 * Mathf.Clamp(relationDistance, 0.0f, 1.0f);
        distanceY = Screen.height / 2 * Mathf.Clamp(relationDistance, 0.0f, 1.0f);

        heightMax = height + height * Mathf.Clamp(relationHeight, 0.0f, 1.0f);
        heightMin = height - height * Mathf.Clamp(relationHeight, 0.0f, 1.0f);
    }

    private void FixedUpdate()
    {
        if (Input.GetButton("Fire2"))
        {
            RotateCamera();
        }
        else
        {
            MouseMovement();
            ArrowMovement();
            VariantSpeed();
            Zoom();
        }
    }

    private void LateUpdate()
    {
        //zoomVariant = (((height - heightMin*0.9f) * 100f) / (heightMax - heightMin*0.9f))/100f;
        speed = (initSpeed + bonusSpeed);
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(this.transform.position,5);
    }

    public void MouseMovement()
    {
        var dif = new Vector3(Input.mousePosition.x - Screen.width / 2, this.transform.position.y, Input.mousePosition.y - Screen.height / 2).normalized * speed * Time.deltaTime;
        nextPosition = this.transform.position + dif;

        RaycastHit hit;
        if (Physics.Raycast(nextPosition, Vector3.down, out hit))
        {
            if (hit.collider.tag.Equals("Border"))
            {
                onMap = false;
            }
            else
            {
                onMap = true;
            }

            this.transform.position = new Vector3(this.transform.position.x, (hit.point.y + height), this.transform.position.z);
        }

        if (onMap)
        {
            if (Mathf.Abs(Input.mousePosition.x - Screen.width / 2) >= distanceX || Mathf.Abs(Input.mousePosition.y - Screen.height / 2) >= distanceY)
            {
                this.transform.Translate(new Vector3(Input.mousePosition.x - Screen.width / 2, 0.0f, Input.mousePosition.y - Screen.height / 2).normalized * speed * Time.deltaTime);
            }
        }
    }

    public void ArrowMovement()
    {
        this.transform.Translate(new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")).normalized * speed * Time.deltaTime);
    }


    public void Zoom()
    {
        if(Input.mouseScrollDelta.y != 0)
        {
           height = Mathf.Clamp((height - Input.mouseScrollDelta.y * scrollSpeed),heightMin,heightMax);
        }
    }

    public void SetVelocityByScroll()
    {
        //set velocity
    }

    public void RotateCamera()
    {
        this.transform.Rotate(new Vector3(0,Input.GetAxis("Mouse X"),0)* rotateSpeed * Time.deltaTime);
    }

    //only work in pc
    public void VariantSpeed()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            bonusSpeed = initSpeed;
        }
        else
        {
            bonusSpeed = 0;
        }
    }
}
