﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// this class is used for determinated the defensorStats
/// </summary>
public class DefensorStats: NetworkBehaviour
{
    public int initialWood = 1000;
    public int initialRock = 1000;
    public int initialFood = 1000;

    public Resource wood;
    public Resource rock;
    public Resource food;

    private DefenderHUD hud;

    bool isInit;

    public void Init()
    {
        wood = new Resource(initialWood, Resource.Type.Wood);
        rock = new Resource(initialRock, Resource.Type.Rock);
        food = new Resource(initialFood, Resource.Type.Food);

        hud = GetComponent<DefenderManager>().Manager.GetComponentInChildren<DefenderHUD>();

        isInit = true;
    }

    void Update()
    {
        if (!isInit)
            return;

        if (hasAuthority)
        {
            hud.Display(this);
        }
    }

}
