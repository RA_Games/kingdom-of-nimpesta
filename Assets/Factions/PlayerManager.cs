﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerManager: MonoBehaviour{

    protected PlayerRuntimeData myPlayer;

    private GameManager gm;

    public GameManager Manager
    {
        get
        {
            if (gm == null)
            {
                gm = FindObjectOfType<GameManager>();
            }

            return gm;
        }
    }

    private void Start()
    {
        myPlayer = GetComponent<PlayerRuntimeData>();
    }

    public virtual void PlanningMode(){}

    public virtual void AssaultMode() { }
}
