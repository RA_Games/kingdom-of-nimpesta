﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour {

    private PlayerRuntimeData myPlayer;
    private GameManager manager;

    private bool ready;
    private Vector3 initCamPos;
    private Quaternion initCamRot;
    private float speed = 700;

    public void StartFollow()
    {
        manager = GetComponent<GameManager>();
        myPlayer = manager.myPlayer;

        initCamPos = Camera.main.transform.position;
        initCamRot = Camera.main.transform.rotation;

        ready = true;
    }

    void LateUpdate ()
    {
        if (!ready)
            return;

        if (myPlayer.faction.Equals(Faction.Viking))
        {
            FollowAttacker();
        }
        else
        {
            if (!ready)
                return;
            
            FollowDefender();
        }
	}

    private void FollowAttacker()
    {
        if (manager.myRoundManager.phase.Equals(RoundManager.Phases.Planning))
        {
            transform.position = Vector3.MoveTowards(transform.position, initCamPos, 300 * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, initCamRot, 50 * Time.deltaTime);
        }
        else
        {
            if (!ready || myPlayer.GetComponent<AttackerManager>().isDead)
                return;

            speed = (Vector3.Distance(transform.position, myPlayer.transform.position) * 700) / Vector3.Distance(initCamPos, myPlayer.transform.position);

            speed = Mathf.Clamp(speed, 50, 700);

            transform.position = Vector3.MoveTowards(transform.position, myPlayer.transform.position - (myPlayer.transform.forward * 3) + (myPlayer.transform.up * 3), speed*Time.deltaTime);

            transform.LookAt(myPlayer.transform.position + (myPlayer.transform.forward * 4));
        }
    }

    private void FollowDefender()
    {
        this.transform.position = Vector3.Lerp(this.transform.position,myPlayer.transform.position,20.0f);
        this.transform.LookAt(myPlayer.transform.position - (myPlayer.transform.up * 4) + (myPlayer.transform.forward * 3));
    }
}
