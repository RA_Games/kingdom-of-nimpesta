﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerManager : PlayerManager {

    public bool isDead = true;

    private Troop _troop;

    public Troop Troop
    {
        get
        {
            if (_troop == null)
            {
               _troop = myPlayer.GetComponent<Troop>();
            }

            return _troop;
        }
    }

    public override void PlanningMode()
    {
        Spawn.ShowSpawns();
        isDead = true;
        Manager.myHud.SetTrigger("Show");
    }

    public void KillMode()
    {
        Spawn.ShowSpawns();
        isDead = true;
        Manager.myHud.SetTrigger("Show");
    }

    public override void AssaultMode()
    {
        SpawnInMap();

        Manager.myHud.SetTrigger("Hide");
    }

    public void SpawnInMap()
    {
        if (Manager.myRoundManager.phase.Equals(RoundManager.Phases.Planning))
            return;

        Manager.status = "Spawning: " + Spawn.selectedSpawn;

        if (Spawn.selectedSpawn == null)
            return;

        Spawn.Hidepawns();
        Troop.RespawnTroop(Spawn.selectedSpawn.GetSpawnPos());
        isDead = false;
    }
}
