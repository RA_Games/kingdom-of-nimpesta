﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerRuntimeData : NetworkBehaviour
{
    [SyncVar]
    public Faction faction;

    [SyncVar]
    public string pname;

    [SyncVar]
    public bool isReady;

    public static PlayerRuntimeData singleton;

    private void Start()
    {
        singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    [Command]
    private void CmdFirstSpawn(int mode)
    {
        if (mode.Equals((int)Faction.Viking))
        {
            GameObject prefab = Resources.Load<GameObject>("GameModes/Attacker/Troop_prefab");
            GameObject player = Instantiate(prefab);

            NetworkServer.ReplacePlayerForConnection(connectionToClient, player, 0);

            TargetChangePlayer(connectionToClient, player);
        }
        else
        {
            GameObject prefab = Resources.Load<GameObject>("GameModes/Defender/Defender");
            GameObject player = Instantiate(prefab);

            NetworkServer.ReplacePlayerForConnection(connectionToClient, player, 0);
            TargetChangePlayer(connectionToClient, player);
        }
    }


    [TargetRpc]
    private void TargetChangePlayer(NetworkConnection target, GameObject newPlayer)
    {
        PlayerRuntimeData newPlayerData = newPlayer.GetComponent<PlayerRuntimeData>();

        newPlayerData.faction = this.faction;
        newPlayerData.pname = this.pname;
    }

    public void SpawnInitalPlayer()
    {
        if (!hasAuthority)
            return;

        CmdFirstSpawn((int)faction);
    }

    public void ActivePlayer()
    {
        if (faction.Equals(Faction.Viking))
        {
            gameObject.AddComponent<AttackerManager>();
        }
        else
        {
            gameObject.AddComponent<DefenderManager>();
        }
    }

    public void SetPlayerReady()
    {
        isReady = true;
    }

    public static Faction GetFaction()
    {
        if (singleton == null)
            return Faction.Viking;

        return singleton.faction;
    }
}

