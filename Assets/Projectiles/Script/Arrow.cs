﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : Proyectile {
    
    // Use this for initialization
    void Start ()
    {
        Invoke("DestroyGarabage", 10);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (collisionDetected)
            return;

        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        CheckCollision();
    }

    public override void CheckCollision()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 0.3f, ((1 << 9) + (1 << 10))))
        {
            IAttacker target = hit.collider.GetComponent<IAttacker>();

            if (target != null)
            {
                target.Death();
                transform.SetParent(hit.collider.transform);
            }
            this.Destroy();
        }
    }

    public override void Destroy()
    {
        Destroy(gameObject);
    }

    public override void OnCollision()
    {
        StartCoroutine(DestroyIt());
    }

    IEnumerator DestroyIt()
    {
        collisionDetected = true;
        yield return new WaitForSeconds(6);
        this.Destroy();
    }
}
