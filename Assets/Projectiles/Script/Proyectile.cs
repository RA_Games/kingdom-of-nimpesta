﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Proyectile : MonoBehaviour {

    public float speed;
    public float step;

    public Vector3 shootPoint;
    public LookOut origen;

    public bool collisionDetected;

    private void Start()
    {
        
    }

    void Update()
    {
          
    }

    private void DestroyGarabage()
    {
        Destroy(gameObject);
    }


    public abstract void CheckCollision();
    public abstract void OnCollision();
    public abstract void Destroy();
}
