﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manage the percentage to generate resource on game
/// </summary>
[RequireComponent(typeof(Region))]
public class FarmResource : MonoBehaviour
{
    public int initialWood;
    public int initialRock;
    public int initialFood;
    public int initialGold;
    private int auxWood;
    private int auxRock;
    private int auxFood;
    private int auxGold;
    private int woodAmount;
    private int rockAmount;
    private int foodAmount;
    private int goldAmount;
    private float coolDown;

    private Region region;
    public DefensorStats defensorStats;
    

    public IEnumerator Start()
    {
        yield return new WaitForSeconds(10); //borrar luego, se tiene que llamar por game manager

        foreach (PlayerRuntimeData prtd in FindObjectsOfType<PlayerRuntimeData>())
        {
            print(prtd.faction);
            if (prtd.faction.Equals(Faction.Anglosaxon))
            {
                defensorStats = prtd.GetComponentInChildren<DefensorStats>();
            }
        }
        region = GetComponent<Region>();
        InitResource(initialWood, initialRock, initialFood, initialGold, region.regionSize);
        coolDown = 5f;
        StartCoroutine(PlusResource());
    }

    public void InitResource(int initialWood, int initialRock, int initialFood, int initialGold, RegionSize regionSize)
    { 
        int delta = (int)(regionSize);
        print(delta);
        auxWood = (int)(Random.Range(-(initialWood * delta / 10f), (initialWood * delta / 10f)));
        auxRock = (int)(Random.Range(-(initialRock * delta / 10f), (initialRock * delta / 10f)));
        auxFood = (int)(Random.Range(-(initialFood * delta / 10f), (initialFood * delta / 10f)));
        auxGold = (int)(Random.Range(-(initialGold * delta / 10f), (initialGold * delta / 10f)));

        woodAmount = initialWood * delta + auxWood;
        rockAmount = initialRock * delta + auxRock;
        foodAmount = initialFood * delta + auxFood;
        goldAmount = initialGold * delta + auxGold;
    }

    IEnumerator PlusResource()
    {
        while (region != null)
        {
            AddValue();
            yield return new WaitForSeconds(coolDown);
        }
    }


    public void AddValue()
    {
        defensorStats.wood.AddAmount(woodAmount);
        defensorStats.rock.AddAmount(rockAmount);
        defensorStats.food.AddAmount(foodAmount);
    }


    public Vector4 GetResourcesPercentage()
    {
        float allResources = initialWood + initialRock + initialFood + initialGold;
        Vector4 resources = Vector4.zero;
        resources.x = initialWood / allResources;
        resources.y = initialRock / allResources;
        resources.z = initialFood / allResources;
        resources.w = initialGold / allResources;

        return resources;
    }
}


