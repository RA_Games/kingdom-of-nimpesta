﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FarmResource))]
[CanEditMultipleObjects]
public class FarmResourceEditor : Editor
{

    public override void OnInspectorGUI()
    {
        FarmResource fr = (FarmResource)target;
        
        fr.initialWood = (int)EditorGUILayout.Slider("Wood", fr.initialWood, 0f, 100f);
        ProgressBar(fr.GetResourcesPercentage().x, "Wood " + (100 * fr.GetResourcesPercentage().x + "%"));
        fr.initialRock = (int)EditorGUILayout.Slider("Rock", fr.initialRock, 0f, 100f);
        ProgressBar(fr.GetResourcesPercentage().y, "Rock " + (100 * fr.GetResourcesPercentage().y + "%"));
        fr.initialFood = (int)EditorGUILayout.Slider("Food", fr.initialFood, 0f, 100f);
        ProgressBar(fr.GetResourcesPercentage().z, "Food " + (100 * fr.GetResourcesPercentage().z + "%"));
        fr.initialGold = (int)EditorGUILayout.Slider("Gold", fr.initialGold, 0f, 100f);
        ProgressBar(fr.GetResourcesPercentage().w, "Gold " + (100 * fr.GetResourcesPercentage().w + "%"));

    }


    void ProgressBar(float value, string label)
    {
        Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField");
        EditorGUI.ProgressBar(rect, value, label);
        EditorGUILayout.Space();

    }


}