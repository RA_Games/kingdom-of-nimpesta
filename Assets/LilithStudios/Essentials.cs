﻿using System;
using UnityEngine;

namespace Essentials
{
	public class Geometry
	{	
        public static Vector3[] CreateCircumferencePosition(int amount, float radius1, float radius2,float height)
        {
            Vector3[] aux = new Vector3[amount];

            for (int i = 0; i < amount; i++)
            {
                aux[i] = new Vector3(radius1 * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), height, radius2 * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i));
            }
            return aux;
        }

        public static Vector3[] CreateCircumferencePosition(int amount, float radius1, float radius2)
        {
            Vector3[] aux = new Vector3[amount];

            for (int i = 0; i < amount; i++)
            {
                aux[i] = new Vector3(radius1 * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), 0.0f, radius2 * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i));
            }
            return aux;
        }

        public static Vector3[] CreateCircumferencePosition(int amount, float radius)
        {
            Vector3[] aux = new Vector3[amount];

            for (int i = 0; i < amount; i++)
            {
                aux[i] = new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), 0.0f, radius * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i));
            }
            return aux;
        }


        public static Mesh CreatePlane(int widht, int height)
		{
			Mesh m = new Mesh();

			Vector3[] vertices = new Vector3[(widht + 1) * (height + 1)];

			for (int i = 0, y = 0; y <= height; y++) {
				for (int x = 0; x <= widht; x++, i++) {
					vertices[i] = new Vector3(x, y);
				}
			}

			m.vertices = vertices;

			int[] triangles = new int[widht * height * 6];
			for (int ti = 0, vi = 0, y = 0; y < height; y++, vi++) {
				for (int x = 0; x < widht; x++, ti += 6, vi++) {
					triangles[ti] = vi;
					triangles[ti + 3] = triangles[ti + 2] = vi + 1;
					triangles[ti + 4] = triangles[ti + 1] = vi + widht + 1;
					triangles[ti + 5] = vi + widht + 2;
				}
			}

			m.triangles = triangles;

			m.RecalculateNormals ();
			m.RecalculateBounds ();

			return m;

		}

		public static float GetAreaPolygon (Vector3 center, Vector3[] points)
		{
			float area = 0;

			for(int i = 0; i < points.Length; i++)
			{
				int next = i + 1;

				if(next < points.Length-1)
				{
					float b = Vector3.Distance (points[i], points[next]);
					float h = Vector3.Distance ((points [i] + points [next]) / 2, center);
					float r = b * h / 2;
					area += r;
				}else
				{
					float b = Vector3.Distance (points[i], points[0]);
					float h = Vector3.Distance ((points [i] + points [0]) / 2, center);
					float r = b * h / 2;
					area += r;
				}
			}

			return area;
		}

		/// <summary>
		/// Checks if is inside polygon.
		/// </summary>
		/// <returns><c>true</c>, if if is inside polygon was checked, <c>false</c> otherwise.</returns>
		/// <param name="origin">Origin.</param>
		/// <param name="point">Point.</param>
		/// <param name="points">Points.</param>
		public static bool CheckIfIsInsidePolygon(Vector3 center, Vector3 point, Vector3[] points)
		{
			Vector3 _min = points[0];
			Vector3 _max = points[0];

			//Phase 1 - Low math calculations
			Vector3 origin = GetCenterOfPolygon (points);

			int closestPointIndex = 0;

			for(int i = 0; i < points.Length; i ++) 
			{
				_min = Vector3.Min(_min, points[i]);
				_max = Vector3.Max(_max, points[i]);
			}

			if(point.x > _min.x && point.x < _max.x)
			{
				if(point.z > _min.z && point.z < _max.z)
				{
					//Phase 2 - Hight math calculations
					//Debug.Log("== Hight math calculations ==");

					int closestIndex = GetClosestVertIndex (points, point);
					int prevIndex = GetPrevVertIndex (closestIndex, points.Length);
					int nextIndex = GetNextVertIndex (closestIndex, points.Length);

					Vector2 p1 = new Vector2(points[closestIndex].x, points[closestIndex].z);
					Vector2 p2_1 = new Vector2(points[prevIndex].x, points[prevIndex].z);

					Vector2 p3 = new Vector2 (point.x, point.z);
					Vector2 p4 = new Vector2 (origin.x, origin.z);

					float tA_1 = (((p3.y-p4.y)*(p1.x-p3.x))+((p4.x-p3.x)*(p1.y-p3.y))) / (((p4.x-p3.x)*(p1.y-p2_1.y))-((p1.x-p2_1.x)*(p4.y-p3.y)));
					float tB_1 = (((p1.y-p2_1.y)*(p1.x-p3.x))+((p2_1.x-p1.x)*(p1.y-p3.y))) / (((p4.x-p3.x)*(p1.y-p2_1.y))-((p1.x-p2_1.x)*(p4.y-p3.y)));

		
					if((tA_1 >= 0 && tA_1 <= 1) && (tB_1 >= 0 && tB_1 <= 1))
					{
						return false;
					}

					Vector2 p2_2 = new Vector2(points[nextIndex].x, points[nextIndex].z);

					float tA_2 = (((p3.y-p4.y)*(p1.x-p3.x))+((p4.x-p3.x)*(p1.y-p3.y))) / (((p4.x-p3.x)*(p1.y-p2_2.y))-((p1.x-p2_2.x)*(p4.y-p3.y)));
					float tB_2 = (((p1.y-p2_2.y)*(p1.x-p3.x))+((p2_2.x-p1.x)*(p1.y-p3.y))) / (((p4.x-p3.x)*(p1.y-p2_2.y))-((p1.x-p2_2.x)*(p4.y-p3.y)));

					if((tA_2 >= 0 && tA_2 <= 1) && (tB_2 >= 0 && tB_2 <= 1))
					{
						return false;
					}

					return true;
				}
			}

			return false;
		}

		public static Vector3 GetClosestVert(Vector3[] verts, Vector3 point)
		{
			float distance = Vector3.Distance (verts[0], point);
			Vector3 closestVert = verts[0];

			foreach(Vector3 vert in verts)
			{
				if (Vector3.Distance (vert, point) < distance) 
				{
					distance = Vector3.Distance (vert, point);
					closestVert = vert;
				}
			}

			return closestVert;
		}

		public static int GetClosestVertIndex(Vector3[] verts, Vector3 point)
		{
			float distance = Vector3.Distance (verts[0], point);
			int index = 0;

			int i = 0;
			foreach(Vector3 vert in verts)
			{
				if (Vector3.Distance (vert, point) < distance) 
				{
					distance = Vector3.Distance (vert, point);
					index = i;
				}

				i++;
			}

			return index;
		}

		/// <summary>
		/// Gets the far verts of polygon:
		/// [0] = Up vert.
		/// [1] = Right vert.
		/// [2] = Down vert.
		/// [3] = Left vert.
		/// </summary>
		/// <returns>The far polygon verts.</returns>
		public static Vector3[] GetFarPolygonVerts(Vector3[] points)
		{         
			Vector3 _min = points[0];
			Vector3 _max = points[0];

			Vector3[] verts = new Vector3[2];

			for(int i = 0; i < points.Length; i ++) 
			{
				_min = Vector3.Min(_min, points[i]);
				_max = Vector3.Max(_max, points[i]);
			}

			verts [0] = _min;
			verts [1] = _max;

			return verts;
		}

		public static int GetNextVertIndex(int currentVert, int maxVerts)
		{
			int nextVert = currentVert + 1;

			if(nextVert > maxVerts-1)
			{
				nextVert = 0;
			}

			return nextVert;
		}

		public static Vector3 GetNextVert(int index, Vector3[] verts)
		{
			return verts [GetNextVertIndex (index, verts.Length)];
		}

		public static int GetPrevVertIndex(int currentVert, int maxVerts)
		{
			int prevVert = currentVert - 1;

			if(prevVert < 0)
			{
				prevVert = maxVerts-1;
			}

			return prevVert;
		}

		public static Vector3 GetPrevVert(int index, Vector3[] verts)
		{
			return verts [GetPrevVertIndex (index, verts.Length)];
		}

		public static Vector3 GetCenterOfPolygon(Vector3[] verts)
		{
			Vector3 center = Vector3.zero;

			for(int i = 0; i < verts.Length; i++)
			{
				center += verts [i];
			}

			center /= verts.Length;

			return center;
		}
	}

	public class MathUtilities{

		/// <summary>
		/// Gets if a point if in the left o right side.
		/// </summary>
		/// <returns><c>true</c>, if direction2 d was gotten, <c>false</c> otherwise.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="origin">Origin.</param>
		public static bool GetSide(float direction, float origin)
		{
			if(direction - origin >= 0)
				return true;

			return false;
		}
	}

    public class Effects
    {
        public static System.Collections.IEnumerator FadeOutMaterial(Material material, float speed)
        {
            Color toFade = material.color;

            while (toFade.a > 0)
            {
                toFade.a -= Time.deltaTime;
                material.color = toFade;
                yield return new WaitForSeconds(Time.deltaTime * speed);
            }

            yield break;
        }
    }
}

