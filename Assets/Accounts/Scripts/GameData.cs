﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData{

    private static Account account;

    public static Account GetAccount()
    {
        if (account == null)
        {
            account = LoadAccount();
        }

        return account;
    }

    private static Account LoadAccount()
    {
        Account account = JsonUtility.FromJson<Account>(Resources.Load<TextAsset>("account").text);

        return account;
    }

    public static void SaveData(Account account)
    {
        
    }
	
}