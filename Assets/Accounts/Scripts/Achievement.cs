﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Achievement{

    public string name;
    public string description;
    public bool isVisible;
    public Texture2D[] images;
    public AchievementType type;
}

[System.Serializable]
public enum AchievementType
{
    Stackeable,
    Specific
}

[System.Serializable]
public class AchievementStackeable : Achievement
{
    private int[] max;
    private int current;
    private int level;

    public AchievementStackeable ()
    {

    }
}

[System.Serializable]
public class AchievementSpecific : Achievement
{
    public bool isUnlocked;

    public AchievementSpecific()
    {

    }
}