﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Quest {

    public string name;
    public string description;
    public Texture2D image;
    public Requisite requisite;
    public Reward reward;
    public DateTime creationDate;
}

[Serializable]
public class Requisite
{

}

[Serializable]
public class Reward
{

}
