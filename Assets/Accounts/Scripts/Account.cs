﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Account
{
    public string username;
    public int id;
    public DateTime lastConection;
    public int gold;
    public int level;
    public float xp;
    public int timePlayed;
    public List<Quest> quests;
    public List<Achievement> achievement;

    public Account(string username, int id)
    {
        this.username = username;
        this.id = id;
        this.lastConection = DateTime.Now;
        this.gold = 0;
        level = 1;
        this.xp = 0;
        this.timePlayed = 1;

        quests = new List<Quest>();
        achievement = new List<Achievement>();
    }
}
