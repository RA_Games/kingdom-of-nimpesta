﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manager of a region.
/// </summary>
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Region : MonoBehaviour, IClickleable {

    public Color originalColor;
	public Faction owner = Faction.Anglosaxon;
    public RegionSize regionSize;
    public List<Zone> zones;

    void LateUpdate()
    {
        if(zones.Count > 0)
            this.CheckConquest();
    }

    public void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && GameManager.Singleton.myPlayer.faction.Equals(Faction.Anglosaxon))
        {
            Click();
        }
        
    }

    /// <summary>
    /// Handle the click event of the region.
    /// </summary>
    public void Click()
    {
        if (GameManager.Singleton.myPlayer.GetComponent<BuilderManager>().IsBuilding())
        {
            return;
        }

        ActionMenuManager.ShowMenu(new IDirectives[]
            {
                Directives.GetBuildTowerDirectives()
            });
    }

    public void CheckConquest ()
    {
        if(owner.Equals(Faction.Anglosaxon))
        {
            foreach (Zone z in zones)
            {
                if (z.GetOwner().Equals(Faction.Anglosaxon))
                {
                    return;
                }
            }

            this.owner = Faction.Viking;
            originalColor = Color.gray;
            return;
        }

        if(owner.Equals(Faction.Viking))
        {
            foreach (Zone z in zones)
            {
                if (z.GetOwner().Equals(Faction.Viking))
                {
                    return;
                }
            }

            this.owner = Faction.Anglosaxon;
            originalColor = Color.green;
            return;
        }
    }
}

public enum RegionSize { Small = 1, Medium = 2, Big = 3 }