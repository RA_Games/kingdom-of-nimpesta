﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// IClickleable is the base class from every clickleable object derives.
/// </summary>
public interface IClickleable {

    void Click();
    void OnMouseOver();
}
