﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LookOut : MonoBehaviour {

    // Use this for initialization
    public GameObject building;
    public Vector3 fireSpot;
    //public float attackSpeed = 0.1f;
    public GameObject proyectile;
    public float fireSpotOffset = 0;
    //public CapsuleCollider coll;
    public Alarm alarm;
    public GameObject target;
    public float fireCooldown;
    public float currentCooldown;
    public float force;
    public float randomArea = 0.1f;

    void Start()
    {
        building = gameObject;

        // <<<<< to se firepot
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = this.transform.position + fireSpot;
        Destroy(sphere.GetComponent<Collider>());

    }

    public void Attack()
    {
        //anim  
        currentCooldown = fireCooldown;
        transform.LookAt(target.transform.position);
        Proyectile arrow = Instantiate(proyectile, this.transform.position + fireSpot, this.transform.rotation * new Quaternion(-Random.Range(-randomArea, randomArea), Random.Range(-randomArea, randomArea), Random.Range(-randomArea, randomArea), 360)).GetComponent<Proyectile>();
    }

}
