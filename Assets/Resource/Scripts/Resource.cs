﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class define the amount of Goods and their functions.
/// </summary>
public class Resource
{
    private int _amount;
    private Sprite icon;

    public enum Type
    {
        Rock, Wood, Food
    }

    private Type categorie;

    public int Amount
    {
        get
        {
            return _amount;
        }
    }

    /// <summary>
    /// the constructor of the class
    /// </summary>
    /// <param name="currentAmount"></param>
    /// <param name="categorie"></param>
	public Resource(int currentAmount, Type categorie)
    {
        this.categorie = categorie;
        this._amount = currentAmount;
    }

    /// <summary>
    /// Use the specified amount this funcion evaluate if the player have enough resources.
    /// </summary>
    /// <param name="amount">Amount.</param>
    public bool CanUse(int amount)
    {
        if (_amount >= amount)
        {
            return (true);
        }
        return (false);
    }

    public void Use(int amount)
    {
        _amount -= amount;
    }

    /// <summary>
    /// add a certain amount to the current amount
    /// </summary>
    /// <param name="amount"></param>
    public void AddAmount(int amount)
    {
        _amount += amount;
    }

    /*
	public Sprite GetIcon ()
	{
		if (icon == null) 
		{
			foreach(Sprite s in Resource<Sprite>("Resources_Icons"))
			{
				if (s.name.Equals ("Resources_Icons_" + categorie.ToString())) 
				{
					icon = s;
					break;
				}
			} 		

			if (icon == null) 
			{
				Debug.Log("[[Warning!]]: can't find the sprite 'Resources_Icons_Rock'inside:" + " Resources/Resources_Icons, please check if the sprite exists or the name match.");

			}

		}

		return icon;
		*/
}
