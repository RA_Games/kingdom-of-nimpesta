﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Zone : NetworkBehaviour
{
    public float conquestPercentage;
    public Renderer center;

    public Image conuest_bar;
    private float attackerConquestPoints;
    private float defenderConquestPoints;
    private Faction owner = Faction.Anglosaxon;

	void Start ()
    {
        conuest_bar = GetComponent<Image>();
        center = GetComponent<Renderer>();

        attackerConquestPoints = 0;
        defenderConquestPoints = 0;
        conquestPercentage = 0;
	}
	
	void LateUpdate ()
    {
        CheckConquest();
	}

    public void OnTriggerStay (UnityEngine.Collider other)
    {
        if (other.gameObject.GetComponent<IAttacker>() != null)
        {
            CmdConquest(other.gameObject.GetComponent<Unit>().conquestPoint);
        }
        else if (other.gameObject.GetComponent<IDefender>() != null)
        {
            CmdRecover(other.gameObject.GetComponent<Unit>().conquestPoint);
        }
    }

    [Command]
    public void CmdRecover(float amount)
    {
        RpcRecover(amount);
    }

    [Command]
    public void CmdConquest(float amount)
    {
        RpcConquest(amount);
    }

    [ClientRpc]
    public void RpcRecover(float amount)
    {
        defenderConquestPoints += (amount / 100000);

        conquestPercentage += attackerConquestPoints - defenderConquestPoints;

        conquestPercentage = Mathf.Clamp(conquestPercentage, 0, 100);

        conuest_bar.fillAmount = conquestPercentage / 100;
    }

    [ClientRpc]
    public void RpcConquest(float amount)
    {
        attackerConquestPoints += (amount / 100000);

        conquestPercentage += attackerConquestPoints - defenderConquestPoints;

        conquestPercentage = Mathf.Clamp(conquestPercentage, 0, 100);

        conuest_bar.fillAmount = conquestPercentage / 100;
    }

    public void CheckConquest ()
    {
        if(owner.Equals(Faction.Anglosaxon))
        {
            if(conquestPercentage >= 75)
            {
                owner = Faction.Viking;
                center.material.color = Color.red;
            }
        }
        else if(owner.Equals(Faction.Viking))
        {
            if(conquestPercentage <= 25)
            {
                owner = Faction.Anglosaxon;
                center.material.color = Color.blue;
            }
        }
    }

    public Faction GetOwner()
    {
        return this.owner;
    }
}