﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Control_LoadinBar : MonoBehaviour {

	public Image loadingBar;
	public int factor;
	public Text feedback;

	public void Start ()
	{
		feedback.text = "Charging...";
		StartCoroutine (Loading ());
	}

	IEnumerator Loading ()
	{
		float percentage = 0;
		while (percentage < 1)
		{
			percentage += Time.deltaTime/factor;
			loadingBar.fillAmount = percentage;
			if (percentage >= 1) 
			{
				feedback.text = "Done";
			}
			yield return new WaitForSeconds (Time.deltaTime);
		}
	}
}
