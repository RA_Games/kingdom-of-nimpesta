﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prototype.NetworkLobby;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Essentials;

public class LoadingScreen : NetworkBehaviour
{
    public Transform contentDefender, contentAttacker;
    public GameObject prefab;
    public Material loadingScreen_material;

    private bool finishLoadingPlayers;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(3);

        StartCoroutine(InitPlayerSlots());

        while (!finishLoadingPlayers)
        {
            yield return null;
        }

        NetworkManager.singleton.ServerChangeScene("TestMap");

        yield return new WaitForSeconds(10);

        PlayerRuntimeData[] playersData = FindObjectsOfType<PlayerRuntimeData>();

        foreach (PlayerRuntimeData player in playersData)
        {
            player.SpawnInitalPlayer();
        }

        yield return new WaitForSeconds(10);

        foreach (PlayerRuntimeData pd in playersData)
        {
            if (isServer)
            {
                NetworkServer.Destroy(pd.gameObject);
            }
        }

        yield return new WaitForSeconds(3);

        FindObjectOfType<GameManager>().InitGameManager();

        StartCoroutine(Essentials.Effects.FadeOutMaterial(loadingScreen_material, 0.5f));

        yield return new WaitForSeconds(15);

        Destroy(this.gameObject);
    }

    private void OnEnable()
    {
        loadingScreen_material.color = Color.white;
    }

    private void OnDisable()
    {
        loadingScreen_material.color = Color.white;
    }

    private IEnumerator InitPlayerSlots()
    {
        foreach (NetworkLobbyPlayer np in LobbyManager.s_Singleton.lobbySlots)
        {
            if (np == null)
            {
                continue;
            }

            LobbyPlayer data = np.GetComponentInChildren<LobbyPlayer>();

            if (data.playerFaction.Equals(Faction.Anglosaxon))
            {
                GameObject slotD = Instantiate(prefab, contentDefender, true);
                slotD.GetComponentInChildren<Text>().text = data.playerName;
            }
            else
            {
                GameObject slot = Instantiate(prefab, contentAttacker, true);
                slot.GetComponentInChildren<Text>().text = data.playerName;
            }

            yield return new WaitForSeconds(0.35f);
        }

        finishLoadingPlayers = true;
    }
}
