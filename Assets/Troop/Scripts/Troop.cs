﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Essentials;

public class Troop : NetworkBehaviour
{
    // arreglar los cagasos de las lista de unidades!

    //prefabs
    public GameObject unit_prefab;
    public GameObject projector_prefab;
   // public Projector projector;

    //troop attributes
    public int maxUnits;
    public TypeOfunits type;
    public Mode mode;
    public List<Vector3> positions;
    public List<Unit> units;

    //movement attributes
    public float speed;
    public float scrollSpeed;
    public Vector3 beforePosition;
    float rotY = 0.0f;
    float rotX = 0.0f;

    //radius attributes
    public float radius;
    public float maxRadius;
    public float minRadius;

    //Spawning
    private Spawn currentSpawn;
    private bool isInit;

    public enum Mode
    {
        attackTower,
        attackUnit,
        attackEverything,
        attackSpecific,
        noAttack
    }
    // defence mode (ataco a lo que me ataque)
    // magnetizar todo incluso unidades aliadas

    public enum TypeOfunits
    {
        soldier,
        Archer,
        Shildmen
    }

    //camera inGame
    private Vector3 moveDirection = Vector3.zero;
    CharacterController controller;

    private float gravity;

    public GameObject attackeble;
    public bool canAttack;
    Vector3 target;

    //gizmos cosas
    Vector3 _attackable;

    private void Start()
    {
        InitTroop();
    }

    private void InitTroop()
	{
        if (!isLocalPlayer)
            return;

        //init
        InitializeUnits();
        SetPositionUnits(units.Count);
        InitializeProjector();
        InitializeRadius();

		beforePosition = this.transform.position;
        gravity = 20.0f;
		
		projector_prefab.GetComponent<Projector>().orthographicSize = radius+1;

        controller = GetComponent<CharacterController>();
        ReorderTroop();

        isInit = true;
    }

    void Update()
    {
        if (!isLocalPlayer)
            return;

        MovementTroop();
        //ChangeRadius();

      /*  
        if (TroopIsDeath())
        {
            CmdRespawn();
            InitializeUnits();
        }
        */
    }

	void LateUpdate()
	{
        MovePositions();
        MovemetProjector();
        MoveUnits();
    }

    public void RespawnTroop(Vector3 position)
    {
        transform.position = position;

        if(!isInit)
        {
            InitTroop();
        }
        else
        {
            //Respawn normal
        }
    }

    void OnTriggerEnter(UnityEngine.Collider other)
    {
        foreach (Unit unit in units)
        {
            unit.Magnet(other.GetComponent<Collider>().gameObject);
        }
    }

    void OnTriggerExit(UnityEngine.Collider other)
    {
        canAttack = false;
        attackeble = null;

        foreach (Unit unit in units)
        {
            if (other.GetComponent<Collider>().gameObject.GetComponent<AtackablePositions>() == unit.magnetizedTo)
            {
                unit.DesMagnetize();
            }
        }
    }

    public void KillUnit(Unit unit)
    {
        unit.DesMagnetize();
        units.Remove(unit);
        positions.RemoveAt(0);
        ReorderTroop();
        Destroy(unit.gameObject);
    }

    public void KillUnit(int index)
    {
        if (units.Count > 0)
        {
            Unit u = units[index];
            units.Remove(u);
            positions.RemoveAt(index);
            ReorderTroop();

            for (int i = 0; i < units.Count; i++)
            {
                units[i].index = i;
            }

            Destroy(u.gameObject);
        }
    }

    [Command]
    public void CmdDeath(NetworkInstanceId id, int index)
    {
        RpcDeath(id, index);
    }

    [ClientRpc]
    public void RpcDeath(NetworkInstanceId id, int index)
    {
        if (netId == id)
        {
            KillUnit(index);
        }
    }

    [Command]
    public void CmdRespawn()
    {
        Spawn spawn = FindObjectOfType<Spawn>();    //Change to player selection

        if (spawn != null)
        {
            Vector3 spawnPos = spawn.transform.position;

            spawnPos.x += Random.Range(-5, 5);
            spawnPos.z += Random.Range(-5, 5);

            gameObject.transform.position = spawnPos;

            foreach (Unit u in units)
            {
                u.TpToPos(transform.position);
            }
        }
    }

    /// <summary>
    /// initialize the unit
    /// </summary>
    private void InitializeUnits()
    {
        switch (type)
        {
            case TypeOfunits.soldier:
                unit_prefab = Resources.Load<GameObject>("Units/Soldier_prefab");
                break;

            case TypeOfunits.Shildmen:
                unit_prefab = Resources.Load<GameObject>("Units/Shildmen_prefab");
                break;

            case TypeOfunits.Archer:
                unit_prefab = Resources.Load<GameObject>("Units/Archer_prefab");
                break;

            default:
                unit_prefab = Resources.Load<GameObject>("Units/Soldier_prefab");
                break;
        }

        InitializePositionUnits(maxUnits);
        InstantiateUnits(maxUnits);
    }

    /// <summary>
    /// initialize the projector
    /// </summary>
    private void InitializeProjector()
    {
        Instantiate(projector_prefab, this.transform.position, Quaternion.Euler(new Vector3(90, 0, 0))).GetComponent<Projector>();
    }

    /// <summary>
    /// Initialize the radius
    /// </summary>
    private void InitializeRadius()
    {
        radius = (maxUnits / 2);
        minRadius = radius * 0.5f;
        maxRadius = radius * 1.5f;
    }

    /// <summary>
    /// set the positions of units in the formation troop
    /// </summary>
    /// <param name="amount"></param>
    private void InitializePositionUnits(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            positions.Add(this.transform.position + new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), 0.0f, radius * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i)));
        }
    }

    /// <summary>
    /// create the units to this troop
    /// </summary>
    /// <param name="amount"></param>
    private void InstantiateUnits(int amount)
    {
        CmdSpawnUnit(amount);
    }
    
    [Command]
    private void CmdSpawnUnit(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject u = Instantiate(unit_prefab, Vector3.one * i, Quaternion.identity);
            NetworkServer.SpawnWithClientAuthority(u, connectionToClient);
            TargetInitUnit(connectionToClient, amount, u, i);
        }
    }

    [TargetRpc]
    void TargetInitUnit(NetworkConnection target, int amount, GameObject u, int i)
    {
        u.GetComponent<Unit>().index = i;
        u.GetComponent<Unit>().troop = this;
        u.transform.position = positions[i];
        u.GetComponent<Unit>().troop.units.Add(u.GetComponent<Unit>());
        ReorderTroop();
    }

    private void GiveOrderPosition()
    {
        for (int i = 0 ; i < units.Count ; i++)
        {
            units[i].objetivePosition = positions[i];
        }
    }



    /// <summary>
    /// move units in this troop
    /// </summary>
    private void MoveUnits()
    {
        int i = 0;
        foreach(Unit u in units)
        {
            if (u.magnetizedTo == null)
            {
                if (positions[i] != null)
                {
                    u.MoveToTargetPosition(positions[i]);
                    i++;
                }
            }
            else
            {
                u.MoveToTargetPosition(u.objetivePosition);
            }
        }
        beforePosition = this.transform.position;
    }


    /// <summary>
    /// reorde de position and the radius on the troop
    /// </summary>
    public void ReorderTroop()
    {
        SetPositionUnits(units.Count);
        radius = ResizeRadius(units.Count);
        minRadius = radius * 0.5f;
        maxRadius = radius * 1.5f;
    }

    /// <summary>
    /// adjust de size of radius troop
    /// </summary>
    /// <param name="amount"></param>
    private float ResizeRadius(int amount)
    {
        return (amount / 2);
    }

    /// <summary>
    /// set the positions of units in the formation troop
    /// </summary>
    /// <param name="amount"></param>
    private void SetPositionUnits(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            positions[i] = this.transform.position + new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), 0.0f, radius * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i));
        }
    }

    /// <summary>
    /// move the positions in the troop in relation of teh position troop
    /// </summary>
    private void MovePositions()
    {
        int i = 0;
        foreach (Vector3 pos in positions)
        {
            positions[i] += this.transform.position - beforePosition;
            i++;
        }
    }

    /// <summary>
    /// reorder posotion in the troop
    /// </summary>
    private void ReorderPosition(int amount)
    {
        int i = 0;
        foreach (Vector3 pos in positions)
        {
            positions[i] = this.transform.position + new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), 0.0f, radius * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i));
            i++;
        }
    }

    /// <summary>
    /// ask if the every unit in the troop died
    /// </summary>
    /// <returns></returns>
    public bool TroopIsDeath()
    {
        if (units.Count <= 0)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Move the PivotTroop.
    /// </summary>
    private void MovementTroop()
	{
        if (controller == null)
            return;

        RotateTroop();

        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
        }

        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    private void RotateTroop()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * 20 * Time.deltaTime;
        rotX += mouseY * 20 * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -80, 80);
        Quaternion Rot = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = Rot;
    }

    /// <summary>
    /// Move the projector
    /// </summary>
	private void MovemetProjector()
    {
		//projector.gameObject.transform.position = Vector3.Lerp (projector.transform.position,this.transform.position + new Vector3(0,1,0),Time.deltaTime*100.0f);
	}

	/// <summary>
	/// Changes the radius.
	/// </summary>
	private void ChangeRadius()
    {
        if (Input.mouseScrollDelta != new Vector2(0, 0))
        {
            SetPositionUnits(units.Count);

            //projector.orthographicSize = radius * 2.0f;
            this.gameObject.GetComponent<CapsuleCollider>().radius = radius;    

            radius = Mathf.Clamp(radius += Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * scrollSpeed, minRadius, maxRadius);
       
        }

    }    
   
}


