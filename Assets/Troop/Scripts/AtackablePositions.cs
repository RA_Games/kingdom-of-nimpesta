﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Essentials;
    
public class AtackablePositions : MonoBehaviour
{
    public int attackerAmount;
    public Vector3[] positions;

    public GameObject[] owns;

    public Form form;
    public float XRadius, YRadius, offset;

    public enum Form
    {
        circle,
        rectangle,
        line
    }

    private void Start()
    {
       
                
        switch(form)
        {
            case Form.circle:

                positions = Geometry.CreateCircumferencePosition(attackerAmount, XRadius);
                for (int i = 0; i < positions.Length; i++)
                {
                    positions[i] += this.transform.position;
                }
               

                //CreateCirclePositions(attackerAmount,XRadius,YRadius);
                break;

            case Form.rectangle:
                CreateRectanglePositions(attackerAmount, XRadius, YRadius);
                break;

            case Form.line:
                CreateLinePositions(attackerAmount, XRadius, offset);
                break;
        }
    }

    /*
    public void CreateCirclePositions(int amount,float radius1, float radius2)
    {
        //adv. esto funcina como circulo, no emos probado mas...
        for (int i = 0; i < amount; i++)
        {
            positions.Add(this.transform.position + new Vector3(radius1 * Mathf.Sin(Mathf.Deg2Rad * (360.0f / amount) * i), 0.0f, radius2 * Mathf.Cos(Mathf.Deg2Rad * (360.0f / amount) * i)));
        }
    }
    */
    public void CreateRectanglePositions(int amount , float radius1, float radius2)
    {
        //poner algo despues
    }

    public void CreateLinePositions(int amount, float radius, float offset)
    {
        positions = new Vector3[amount];

        for (int i = 0; i < amount; i++)
        {
            positions[i] = (this.transform.position + this.transform.forward * (radius * i / (amount - 1) - radius / 2) + (transform.right * offset));
        }
    }
}
