﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Text username_text;
    public Text gold_text;
    public Text level_text;
    public Text xp_text;

    private Account account;

    private void Start()
    {
        account = GameData.GetAccount();

        username_text.text = account.username + "#" + account.id;
        gold_text.text = account.gold.ToString();
        level_text.text = account.level.ToString();
    }

    public void LoadLobby()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("NetworkLobby");
    }
}
