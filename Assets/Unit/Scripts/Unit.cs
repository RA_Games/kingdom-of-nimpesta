﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Unit : NetworkBehaviour, IAttacker
{
    public GameObject Instance { get; set; }

    [System.NonSerialized]
    public Troop troop;

    //stats atrivutes
    public float defense;
    public float evasion;
    public int damage;
    public int index;
    
    [Range(0,1)]
    public float conquestPoint;
    public Vector3 targetPos;

    public Rigidbody rb;

    public List<IDefender> onTargets;
    public float distancesToAttack;

    public Vector3 objetivePosition;
    public AtackablePositions magnetizedTo;

    public float Cooldown = 5;
    public float CurrentCooldown;

    public void Start()
    {
        Instance = this.gameObject;
        rb= this.gameObject.GetComponent<Rigidbody>();
        Instance = gameObject;
    }

    public void Update()
    {
        if (CurrentCooldown > 0)
        {
            CurrentCooldown -= Time.deltaTime;
        }
        else
        {
            Attack();
        }
    }

    public void LateUpdate()
    {
        //MoveToTargetPosition(objetivePosition);
    }

    public void Death()
    {
        if (troop.hasAuthority)
           troop.CmdDeath(troop.netId, index);
    }

    // non implement
    public void Block()
    {
        if (!CanAvoidDamage(defense))
        {
            troop.CmdDeath(troop.netId, index);
        }
        else
        {
            //anim block
        }

    }

    // non implement
    public void Evade()
    {
        if (!CanAvoidDamage(evasion))
        {
            troop.CmdDeath(troop.netId, index);
        }
        else
        {
            //anim evade
        }
    }

    /// <summary>
    /// check if this can avoid the damage
    /// **percentage is 0 to 100**
    /// </summary>
    /// <param name="percentage"></param>
    /// <returns></returns>
    private bool CanAvoidDamage(float percentage)
    {
        percentage = Mathf.Clamp(percentage, 0, 100);
        if (Random.Range(0, 100) >= percentage)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// magnetize to the target
    /// </summary>
    /// <param name="target"></param>
    public void Magnet(GameObject target)
    {
        AtackablePositions magnet = target.GetComponent<AtackablePositions>();
        if (magnet == null)
        {
            return;
        }
       
        float shortest = Mathf.Infinity;
        int index = 0;

        if (magnetizedTo == null)
        {
           
            int i = 0;
            foreach (GameObject own in magnet.owns)
            {
               
                if (own == null && shortest >= Vector3.Distance(magnet.positions[i] , this.transform.position))
                {
                    shortest = Vector3.Distance(magnet.positions[i], this.transform.position);
                    objetivePosition = magnet.positions[i];
                    index = i;
                }
                i++;
                
            }

            if (objetivePosition == null)
            {
                return;
            }

            magnetizedTo = magnet;
            magnet.owns[index] = this.gameObject;
           
        }

    }

    /// <summary>
    /// desmagnetize this unit
    /// </summary>
    public void DesMagnetize()
    {
        if (magnetizedTo == null )
        {
            return;
        }

        for(int i = 0 ; i < magnetizedTo.owns.Length ; i++)
        {
            if (magnetizedTo.owns[i] == this.gameObject)
            {
                magnetizedTo.owns[i] = null;
                magnetizedTo = null;
                break;
            }
        }
        //objetivePosition = Vector3.zero; //change afterwards***********
    }

    /// <summary>
    /// move this unit to somewhere
    /// </summary>
    /// <param name="targetPosition"></param>
    public void MoveToTargetPosition(Vector3 targetPosition)
    {
        Vector3 target = new Vector3(targetPosition.x, this.transform.position.y, targetPosition.z);
        this.transform.position = Vector3.MoveTowards(this.transform.position,target, troop.speed * Time.deltaTime);
    }

    public void TpToPos(Vector3 targetPosition)
    {
        this.transform.position = targetPosition;
    }

    protected abstract void Attack();



}
