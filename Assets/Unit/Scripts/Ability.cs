﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour{

	private float duration;
	private float currentColdDown;
	private float maxColdDown;
	private Sprite icon;
	private bool learned;

	Ability(float duration,float maxColdDown,float currentColdDown){
		this.duration = duration;
		this.maxColdDown = maxColdDown;
		this.currentColdDown = currentColdDown;
	}

	public bool IsUsable(float currentColdDown){
		if (currentColdDown <= 0 && learned) {
			return true;
		}
		return false;
	}

	public bool IsLearned(){
		return learned;
	}

	public bool LearnAbility(){
		if (learned) {
			return false;
		}
		this.learned = true;
		return true;
	}

    public bool CanBlock(int percentage)
    {
        percentage = Mathf.Clamp(percentage, 0, 1);
        if (Random.Range(0, 1) >= percentage)
        {
            return true;
        }
        return false;
    }

}
