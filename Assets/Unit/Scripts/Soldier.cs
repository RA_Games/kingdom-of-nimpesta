﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : Unit
{
    
    protected override void Attack()
    {

        if (magnetizedTo != null && magnetizedTo.GetComponent<IDefender>() != null)
        {
            if (Vector3.Distance(objetivePosition, this.transform.position) <= 1.0f)
            {
                //anim
                magnetizedTo.GetComponent<Constructible>().TakeDamage(damage);
                print(magnetizedTo.GetComponent<Constructible>().currentHealth);
                CurrentCooldown = Cooldown;
                if (magnetizedTo.GetComponent<Constructible>().currentHealth <= 0)
                {
                    troop.canAttack = false;
                }
            }
        }


    }


}
