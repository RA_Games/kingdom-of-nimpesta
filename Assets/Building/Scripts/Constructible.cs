﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// this class is the main componet of all constructible components
/// </summary>
[System.Serializable]
public abstract class Constructible : NetworkBehaviour
{
    //public GameObject prefab;
    [Range(0, 1000)]
    public int rockCost;
    [Range(0, 1000)]
    public int woodCost;
    [Range(0, 1000)]
    public int foodCost;
    [Range(0, 3000)]
    public int maxHealth;
    [SyncVar]
    public int currentHealth;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public int GetSellRockAmount()
    {
        return rockCost / 2;
    }

    public int GetSellFoodAmount()
    {
        return foodCost / 2; 
    }

    public int GetSellWoodAmount()
    {
        return foodCost / 2;
    }

    /// <summary>
    /// this metod is used for destroid Constructions in base a damege
    /// </summary>
    /// <param name="damage"></param>
    public abstract void ConstructibleDestruction();

    public void TakeDamage(int damageTaken)
    {
        currentHealth -= damageTaken;
        if(currentHealth <= 0)
        {
            ConstructibleDestruction();
        }
    }

}
