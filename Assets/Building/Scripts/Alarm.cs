﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Alarm : MonoBehaviour {

    public List<LookOut> lookOuts;

    public List<IAttacker> attakers = new List<IAttacker>();

    public Vector3 debug_hitPoint;

    private void Update()
    {
        GiveTarget();
        foreach(LookOut lo in lookOuts)
        {
            lo.currentCooldown -= Time.deltaTime;
            if(lo.currentCooldown <= 0 && lo.target != null )
            {
                lo.Attack();
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        AddAttacker(other.GetComponent<Collider>().gameObject);

    }

    public void OnTriggerExit(Collider other)
    {
        RemoveAttacker(other.GetComponent<Collider>().gameObject);
    }

    public void AddAttacker(GameObject target)
    {
        var objetive = target.gameObject.GetComponent<IAttacker>();

        if (objetive == null)
        {
            return;
        }

        if (!attakers.Contains(objetive))
        {
            attakers.Add(objetive);
        }
    }

    public void RemoveAttacker(GameObject target)
    {
        var objetive = target.gameObject.GetComponent<IAttacker>();

        if (objetive == null)
        {
            return;
        }

        if (!attakers.Contains(objetive))
        {
            attakers.Remove(objetive);
        }
    }

    public void GiveTarget()
    {
        foreach(LookOut lo in lookOuts)
        {
            float shortest = Mathf.Infinity;
            foreach(IAttacker a in attakers)
            {
                if (CanSee(lo.gameObject.transform.position, a.Instance))
                {
                    if(shortest >= Vector3.Distance(lo.gameObject.transform.position, a.Instance.transform.position))
                    {
                        shortest = Vector3.Distance(lo.gameObject.transform.position, a.Instance.transform.position);
                        lo.target = a.Instance;
                        
                    }
                }
            }
        }
    }

    public bool CanSee(Vector3 viewer, GameObject attaker)
    {
        if (attaker == null)
            return false;

        RaycastHit hit;
        Ray ray = new Ray(viewer, attaker.transform.position);

        if (Physics.Linecast(ray.origin, attaker.transform.position, out hit))
        {
            if (hit.collider.gameObject.GetInstanceID() == attaker.GetInstanceID())
            {
                return true;
            }
        }
        return false;
    }







}
