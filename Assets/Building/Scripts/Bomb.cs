﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Proyectile {

    public GameObject explotionPrefab;

    private void Update()
    {
        CheckCollision();
    }

    public override void CheckCollision()
    {
        if (collisionDetected)
            return;

        transform.Translate(Vector3.forward * speed * Time.deltaTime);

        if (Physics.Raycast(transform.position, transform.forward, 0.3f))
        {
            OnCollision();
        }
    }

    public override void Destroy()
    {
        Destroy(gameObject);
    }

    public override void OnCollision()
    {
        Instantiate(explotionPrefab,transform.position,Quaternion.identity);
        Destroy(gameObject);
    }
}
