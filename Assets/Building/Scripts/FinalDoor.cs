﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDoor : Constructible, IDefender
{
    public FinalDoor singleton;

    // Update is called once per frame


    public void EndGame()
    {
        {
            //set speed of all to 0
            //anim end game
            //go to lobby
        }
    }

    public override void ConstructibleDestruction()
    {
        if (!isServer)
        {
            return;
        }
        //anim
        EndGame();
    }
}
