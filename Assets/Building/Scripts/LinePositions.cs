﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePositions : MonoBehaviour {

    public List<Vector3> positions;
    public float radius1, radius2, height, offset;
    public int amount;

	// Use this for initialization
	void Start () {
        CreateLinePositions(amount, radius1, radius2, height, offset);
        for(int i = 0; i<positions.Count; i++)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = positions[i];
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateLinePositions(int amount, float radius1, float radius2, float height, float offset)
    {
        float radius;
        if(radius1>radius2)
        {
            radius = radius1;
        }
        else
        {
            radius = radius2;
        }
        for (int i = 0; i < amount; i++)
        {
            positions.Add(this.transform.position + this.transform.forward * (radius * i / (amount - 1) - radius / 2) + (transform.right * offset));
        }
    }
}
