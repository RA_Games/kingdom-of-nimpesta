﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Essentials;

/// <summary>
/// this is the main component of each tower
/// </summary>
public abstract class Tower : Constructible, IDefender 
{
    public int lookOutAmount;
    public Vector3[] positions;
    public float radius;
    public float height;
    public GameObject lookOut_prefab;
    public Alarm alarm;
    public float buildSpeed;


    void Start()
    {
        this.CreateLookOutsPositions();
        this.gameObject.GetComponent<Animator>().SetFloat("BuildSpeed", buildSpeed);
    }

    public void InstanciateLookOut()
    {
        int i = 0;
        foreach (Vector3 pos in positions)
        {
            var aux = Instantiate(lookOut_prefab,positions[i], Quaternion.identity);
            alarm.lookOuts.Add(aux.GetComponent<LookOut>());
            aux.GetComponent<LookOut>().alarm = this.alarm;
            aux.transform.SetParent(this.transform);
            i++;
        }
    }

    public void CreateLookOutsPositions()
    {
        positions = Geometry.CreateCircumferencePosition(lookOutAmount, radius, radius, height);
        for (int i = 0; i < positions.Length; i++)
        {
            positions[i] += this.transform.position;
        }
    }

    public override void ConstructibleDestruction()
    {
        if (!isServer)
        {
            return;
        }
        Destroy(this.gameObject);
    }
}
