﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearTrap : Constructible {


    public override void ConstructibleDestruction()
    {
        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        print("activada");
        if(other.GetComponent<IAttacker>() != null)
        {
            print("se muere");
            other.GetComponent<IAttacker>().Death();
            this.gameObject.GetComponent<Collider>().enabled = false;
        }
        //ConstructibleDestruction();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
