﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Constructible {

    public GameObject explosionPrefab;

    public void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IAttacker>() != null)
            return;
        Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

    public override void ConstructibleDestruction()
    {

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
