﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class HealthBarManager : NetworkBehaviour
{

    public Image healthBar;
    public Image healthBackground;
    public Constructible building;


	// Use this for initialization
	void Start ()
    {
        building = GetComponent<Constructible>();
       // healthBar.fillAmount = 1;
	}
	
	// Update is called once per frame
	void Update ()
    {
       // building.currentHealth--;
	}

    public void ActualizeHealthBar ()
    {
        if(building != null)
        {
            healthBar.fillAmount = building.currentHealth / building.maxHealth;
        }
    }
}
