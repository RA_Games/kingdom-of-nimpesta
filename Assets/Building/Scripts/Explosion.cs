﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    public float explotionDuration;
    Color color;
    public float f = 1;
	// Use this for initialization
	void Start ()
    {
        color = transform.GetComponent<Renderer>().material.color;
        StartCoroutine(Explode());
	}
	
	// Update is called once per frame
	void Update () {
        print(f);
        f -= Time.deltaTime/explotionDuration;
        color.a = f;
        transform.GetComponent<Renderer>().material.color = color;
    }


    public void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Unit>() != null)
        {
            other.GetComponent<Unit>().Death();
        }
        print("Boom");
    }

    IEnumerator Explode()
    {
        yield return new WaitForSeconds(explotionDuration);
        Destroy(gameObject);
    }
}
