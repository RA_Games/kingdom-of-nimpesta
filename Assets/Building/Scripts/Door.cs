﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Constructible , IDefender{


    public Animation open;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void ConstructibleDestruction ()
    {
        if (!isServer)
        {
            return;
        }
        //anim
        this.gameObject.GetComponent<Animator>().SetBool("broken", true);
    }
}
