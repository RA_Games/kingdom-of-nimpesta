﻿using UnityEngine;
using UnityEditor;

public class ReverseNormals : EditorWindow
{

    private string error = "";

    [MenuItem("Window/Reverse Normals")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(ReverseNormals));
    }

    void OnGUI()
    {
        //Transform curr = Selection.activeTransform;
        GUILayout.Label("Creates a clone of the game object with with\n" +
            "the normals reversed");
        GUILayout.Space(20);

        if (GUILayout.Button("Reverse Normals"))
        {
            error = "";
            ReverseTheNormals();
        }

        GUILayout.Space(20);
        GUILayout.Label(error);
    }

    void ReverseTheNormals()
    {
        Transform curr = Selection.activeTransform;

        MeshFilter mf;
        if (curr == null)
        {
            error = "No appropriate object selected.";
            Debug.Log(error);
            return;
        }

        mf = curr.GetComponent<MeshFilter>();
        if (mf == null || mf.sharedMesh == null)
        {
            error = "No mesh on the selected object";
            Debug.Log(error);
            return;
        }

        // Create the duplicate game object
        GameObject go = Instantiate(curr.gameObject) as GameObject;
        mf = go.GetComponent<MeshFilter>();
        mf.sharedMesh = Instantiate(mf.sharedMesh) as Mesh;
        curr = go.transform;

        // Fix the normals
        Vector3[] normals = mf.sharedMesh.normals;
        if (normals != null)
        {
            for (int i = 0; i < normals.Length; i++)
                normals[i] = -normals[i];
        }
        mf.sharedMesh.normals = normals;

        // Reverse the triangles
        for (int i = 0; i < mf.sharedMesh.subMeshCount; i++)
        {
            int[] triangles = mf.sharedMesh.GetTriangles(i);
            for (int j = 0; j < triangles.Length; j += 3)
            {
                int temp = triangles[j];
                triangles[j] = triangles[j + 1];
                triangles[j + 1] = temp;
            }
            mf.sharedMesh.SetTriangles(triangles, i);
        }

        // Set selection to new game object
        Selection.activeObject = curr.gameObject;


        // Save a copy to disk
        string name = "Assets/Editor/" + go.name + Random.Range(0, int.MaxValue).ToString() + ".asset";
        AssetDatabase.CreateAsset(mf.sharedMesh, name);
        AssetDatabase.SaveAssets();
    }
}