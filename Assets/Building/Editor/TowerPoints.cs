﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TowerPoints : MonoBehaviour {

    public List<Vector3> points = new List<Vector3>();

}

[CustomEditor(typeof(TowerPoints))]
public class TowerPointsEditor : Editor
{

    public override void OnInspectorGUI()
    {
        TowerPoints tp = (TowerPoints)target;

        if (GUILayout.Button("Add"))
        {
            tp.points.Add(Vector3.zero);
        }

        if (GUILayout.Button("Remove") && tp.points.Count > 0)
        {
            tp.points.RemoveAt(tp.points.Count-1);
        }

        foreach (Vector3 v in tp.points)
        {
            EditorGUILayout.Vector3Field("Points", v);
        }
    }

}

