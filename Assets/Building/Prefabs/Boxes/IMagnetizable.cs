﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMagnetizable
{
    void CreatePosition(int positionsAmount, float width, float heigth, float depth, float offset);
}
