﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour , IMagnetizable, IDefender
{
    public int positionsAmount;
    public List<Vector3> positions;
    public const int maxHealth = 100;
    public int health = maxHealth;
    public ParticleSystem part;

    // Use this for initialization
    void Start ()
    {
        if (GetComponent<BoxCollider>() != null)
        {
            BoxCollider col = GetComponent<BoxCollider>();
            float depth = col.bounds.size.z;
            float width = col.bounds.size.x;
            float heigth = col.bounds.size.y;
            this.CreatePosition(this.positionsAmount, width, heigth, depth, 1);
        }

        part.Stop();
	}
	    
	// Update is called once per frame
	void Update () {
		
	}

    public void CreatePosition(int positionsAmount, float width, float heigth, float depth, float offset)
    {
        for(int i = 0; i < positionsAmount; i++)
        {
            float n = (positionsAmount) / 4;
            float x = (width/2)* Mathf.Cos(Mathf.Deg2Rad * (360.0f / positionsAmount) * i);
            float z = (depth/2)* Mathf.Sin(Mathf.Deg2Rad * (360.0f / positionsAmount) * i);
            
            if((360.0f / positionsAmount) * i < 135 && (360.0f / positionsAmount) * i > 45)
            {
                z = -((depth / 2) + offset);
            }
            else if((360.0f / positionsAmount) * i < 225 && (360.0f / positionsAmount) * i > 135)
            {
                x = -((width / 2) + offset);
            }
            else if((360.0f / positionsAmount) * i < 325 && (360.0f / positionsAmount) * i > 225)
            {
                z = (depth / 2) + offset;
            }
            else
            {
                x = (width / 2) + offset;
            }
            

            positions.Add(transform.position + new Vector3(x, 0.0f, z));
           /* GameObject p = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            p.transform.position = positions[i];
            p.GetComponent<Renderer>().material.color = Color.red;*/
        }
    }

    public void TakeDamage(int amount)
    {
        health -= amount;
        if (health <= 0)
        {
            health = 0;
            StartCoroutine(BoxDeath());
        }
    }

    IEnumerator BoxDeath()
    {
        Destroy(gameObject);
        part.Play();
        yield return new WaitForSeconds(0.3f);
        part.Stop();
    }
}
