﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The directives manage the actions from the "ActionMenu". You can add more directives with the same pattern.
/// </summary>
public static class Directives {

    private static BuildDirective buildDirective;
    private static InformationDirective informationDirective;
    private static AssaultDirective assaultDirective;
    private static BuildTowerDirective buildTowerDirective;

    /// <summary>
    /// Returns the BuildDirective, if it's null, then create a new one and return it.
    /// </summary>
    /// <returns></returns>
    public static BuildDirective GetBuildDirective()
    {
        if (buildDirective == null)
        {
            buildDirective = new BuildDirective();
        }

        return buildDirective;
    }

    /// <summary>
    /// Returns the InformationDirective, if it's null, then create a new one and return it.
    /// </summary>
    /// <returns></returns>
    public static InformationDirective GetInformationDirective()
    {
        if (informationDirective == null)
        {
            informationDirective = new InformationDirective();
        }

        return informationDirective;
    }

    /// <summary>
    /// Returns the AssaultDirective, if it's null, then create a new one and return it.
    /// </summary>
    /// <returns></returns>
    public static AssaultDirective GetAssaultDirective()
    {
        if (assaultDirective == null)
        {
            assaultDirective = new AssaultDirective();
        }

        return assaultDirective;
    }

    /// <summary>
    /// Returns the BuildTowerDirective, if it's null, then create a new one and return it.
    /// </summary>
    /// <returns></returns>
    public static BuildTowerDirective GetBuildTowerDirectives()
    {
        if (buildTowerDirective == null)
        {
            buildTowerDirective = new BuildTowerDirective();
        }

        return buildTowerDirective;
    }

    public class BuildTowerDirective : IDirectives
    {
        public Sprite icon { get; set; }
        public Texture texture { get; set; }

        /// <summary>
        /// Generates a new directive and find the corresponding icon in "Resources/DirectivesUI" folder.
        /// </summary>
        public BuildTowerDirective()
        {
            bool isIconFounded = false;
            foreach (Sprite s in Resources.LoadAll<Sprite>("DirectivesUI"))
            {
                if (s.name == "Directives_Build")
                {
                    icon = s;
                    isIconFounded = true;
                    break;
                }
            }

            if (!isIconFounded)
            {
                Debug.Log("[[Warning!]]: can't find the sprite 'Directives_Build'inside:" + "Resources/DirectivesUI, please check if the sprite exists or the name match.");
            }
        }

        /// <summary>
        /// Execute the directive.
        /// </summary>
        public void Select()
        {
            BuilderManager.Singleton.SelectTowerToBuild(TowerTypes.Arrow_Tower);
            BuilderManager.Singleton.ActiveSpawn();
        }

        /// <summary>
        /// Get the texture, if it's null, then create a new one.
        /// </summary>
        /// <returns></returns>
        public Texture GetTexture()
        {
            if (texture == null)
            {
                var croppedTexture = new Texture2D((int)icon.rect.width, (int)icon.rect.height);
                var pixels = icon.texture.GetPixels((int)icon.textureRect.x,
                                                        (int)icon.textureRect.y,
                                                        (int)icon.textureRect.width,
                                                        (int)icon.textureRect.height);
                croppedTexture.SetPixels(pixels);
                croppedTexture.Apply();

                texture = croppedTexture;
                return croppedTexture;
            }

            return texture;
        }
    }

    /// <summary>
    /// Directive of the build action button.
    /// </summary>
    public class BuildDirective : IDirectives
    {
        public Sprite icon { get; set; }
        public Texture texture { get; set; }

        /// <summary>
        /// Generates a new directive and find the corresponding icon in "Resources/DirectivesUI" folder.
        /// </summary>
        public BuildDirective()
        {
            bool isIconFounded = false;
            foreach (Sprite s in Resources.LoadAll<Sprite>("DirectivesUI"))
            {
                if (s.name == "Directives_Build")
                {
                    icon = s;
                    isIconFounded = true;
                    break;
                }
            }

            if (!isIconFounded)
            {
                Debug.Log("[[Warning!]]: can't find the sprite 'Directives_Build'inside:" + "Resources/DirectivesUI, please check if the sprite exists or the name match.");
            }
        }

        /// <summary>
        /// Execute the directive.
        /// </summary>
        public void Select()
        {
            BuilderManager.Singleton.ActiveSpawn();
        }

        /// <summary>
        /// Get the texture, if it's null, then create a new one.
        /// </summary>
        /// <returns></returns>
        public Texture GetTexture()
        {
            if (texture == null)
            {
                var croppedTexture = new Texture2D((int)icon.rect.width, (int)icon.rect.height);
                var pixels = icon.texture.GetPixels((int)icon.textureRect.x,
                                                        (int)icon.textureRect.y,
                                                        (int)icon.textureRect.width,
                                                        (int)icon.textureRect.height);
                croppedTexture.SetPixels(pixels);
                croppedTexture.Apply();

                texture = croppedTexture;
                return croppedTexture;
            }

            return texture;
        }
    }

    /// <summary>
    /// Directive of the information action button.
    /// </summary>
    public class InformationDirective: IDirectives
    {
        public Sprite icon { get; set; }
        public Texture texture { get; set; }

        /// <summary>
        /// Generates a new directive and find the corresponding icon in "Resources/DirectivesUI" folder.
        /// </summary>
        public InformationDirective()
        {
            bool isIconFounded = false;
            foreach (Sprite s in Resources.LoadAll<Sprite>("DirectivesUI"))
            {
                if (s.name == "Directives_Information")
                {
                    icon = s;
                    isIconFounded = true;
                    break;
                }
            }

            if (!isIconFounded)
            {
                Debug.Log("[[Warning!]]: can't find the sprite 'Directives_Information'inside:" + "Resources/DirectivesUI, please check if the sprite exists or the name match.");
            }
        }

        /// <summary>
        /// Execute the directive.
        /// </summary>
        public void Select()
        {

        }

        /// <summary>
        /// Get the texture, if it's null, then create a new one.
        /// </summary>
        /// <returns></returns>
        public Texture GetTexture()
        {
            if (texture == null)
            {
                var croppedTexture = new Texture2D((int)icon.rect.width, (int)icon.rect.height);
                var pixels = icon.texture.GetPixels((int)icon.textureRect.x,
                                                        (int)icon.textureRect.y,
                                                        (int)icon.textureRect.width,
                                                        (int)icon.textureRect.height);
                croppedTexture.SetPixels(pixels);
                croppedTexture.Apply();

                texture = croppedTexture;
                return croppedTexture;
            }

            return texture;
        }
    }

    /// <summary>
    /// Directive of the assault action button.
    /// </summary>
    public class AssaultDirective: IDirectives
    {
        public Sprite icon { get; set; }
        public Texture texture { get; set; }

        /// <summary>
        /// Generates a new directive and find the corresponding icon in "Resources/DirectivesUI" folder.
        /// </summary>
        public AssaultDirective()
        {
            bool isIconFounded = false;

            foreach (Sprite s in Resources.LoadAll<Sprite>("DirectivesUI"))
            {
                if (s.name == "Directives_Assault")
                {
                    icon = s;
                    isIconFounded = true;
                    break;
                }
            }

            if (!isIconFounded)
            {
                Debug.Log("[[Warning!]]: can't find the sprite 'Directives_Assault'inside:" + "Resources/DirectivesUI, please check if the sprite exists or the name match.");
            }
        }

        /// <summary>
        /// Execute the directive.
        /// </summary>
        public void Select()
        {

        }

        /// <summary>
        /// Get the texture, if it's null, then create a new one.
        /// </summary>
        /// <returns></returns>
        public Texture GetTexture()
        {
            if (texture == null)
            {
                var croppedTexture = new Texture2D((int)icon.rect.width, (int)icon.rect.height);
                var pixels = icon.texture.GetPixels((int)icon.textureRect.x,
                                                        (int)icon.textureRect.y,
                                                        (int)icon.textureRect.width,
                                                        (int)icon.textureRect.height);
                croppedTexture.SetPixels(pixels);
                croppedTexture.Apply();

                texture = croppedTexture;
                return croppedTexture;
            }

            return texture;
        }
    }
}
