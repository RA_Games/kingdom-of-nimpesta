﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// IDirectives is the base class from every directive action derives.
/// </summary>
public interface IDirectives {

    Sprite icon { get; set; }
    Texture texture { get; set; }

    void Select();
    Texture GetTexture();
}
