﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manager for the action menu.
/// </summary>
public class ActionMenuManager: MonoBehaviour
{
    private static bool isShowing;
    private bool isCreated;

    private static ActionMenu currentMenu;
    private static Vector2 lastMousePos;
    private static float delta;
    private static BuilderManager singleton;

    /// <summary>
    /// Show a default menu.
    /// </summary>
    public static void ShowMenu()
    {
        currentMenu = new ActionMenu(new Directives.InformationDirective());
        singleton.DisableSpawn();
        isShowing = true;
        lastMousePos = Input.mousePosition;
        delta = 0;
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, 0);
    }

    /// <summary>
    /// Show a custom menu.
    /// </summary>
    /// <param name="directives"></param>
    public static void ShowMenu(IDirectives[] directives)
    {
        currentMenu = new ActionMenu(directives);
        isShowing = true;
        lastMousePos = Input.mousePosition;
        delta = 0;
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, 0);
    }

    private void OnGUI() 
    {
        if (isShowing)
        {
            GenerateMenu();
        }
        else
        {
            if(currentMenu != null)
                HideMenu();
        }
    }

    /// <summary>
    /// Build a menu with the respectives directives and manage the animation.
    /// </summary>
    private void GenerateMenu()
    {   
        delta += 0.03f;
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, Mathf.Clamp01(delta));

        int i = 0;
        foreach (IDirectives dir in currentMenu.GetDirectives())
        {
            Rect rect = new Rect(lastMousePos.x + Mathf.Cos(Mathf.Clamp01(Mathf.Clamp(delta, 0, 70)) * (i + (i + 1))) * 100, (Screen.height - lastMousePos.y) + Mathf.Sin(Mathf.Clamp01(Mathf.Clamp(delta, 0, 70)) * (i + (i + 1))) * 100, 50, 50);

            if (GUI.Button(rect, dir.GetTexture()))
            {
                dir.Select();
                isShowing = false;
                return;
            }

            i++;
        }

        if (Input.GetMouseButton(1))
        {
            isShowing = false;
        }
    }

    /// <summary>
    /// Hide the menu with an a animation.
    /// </summary>
    private void HideMenu()
    {
        delta -= 0.03f;

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, Mathf.Clamp01(delta));

        int i = 0;
        foreach (IDirectives dir in currentMenu.GetDirectives())
        {
            Rect rect = new Rect(lastMousePos.x + Mathf.Cos(Mathf.Clamp01(Mathf.Clamp(delta, 0, 70)) * (i + (i + 1))) * 100, (Screen.height - lastMousePos.y) + Mathf.Sin(Mathf.Clamp01(Mathf.Clamp(delta, 0, 70)) * (i + (i + 1))) * 100, 50, 50);

            if (GUI.Button(rect, dir.GetTexture())) { }

            i++;
        }
    }
}

/// <summary>
/// Main class for the ActionMenuManager, manage the actions for the action menu.
/// </summary>
public class ActionMenu {

    private IDirectives[] directives;

    /// <summary>
    /// Contructor to crate the action menu with one directive.
    /// </summary>
    /// <param name="directive"></param>
    public ActionMenu(IDirectives directive)
    {
        this.directives = new IDirectives[1];
        this.directives[0] = directive;
    }

    /// <summary>
    /// Contructor to crate the action menu with many directives.
    /// </summary>
    /// <param name="directives"></param>
    public ActionMenu(IDirectives[] directives)
    {
        this.directives = new IDirectives[directives.Length];
        this.directives = directives;
    }

    /// <summary>
    /// Obtain the respective directives from this action menu.
    /// </summary>
    /// <returns></returns>
    public IDirectives[] GetDirectives()
    {
        return directives;
    }

    /// <summary>
    /// Get a specific directive from this action menu.
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public IDirectives GetDirective(int i)
    {
        if (i < GetDirectivesSize())
            return directives[i];
        else
            return directives[0];
    }

    /// <summary>
    /// Get the length of the directives from this action menu.
    /// </summary>
    /// <returns></returns>
    public int GetDirectivesSize()
    {
        return directives.Length;
    }

}
